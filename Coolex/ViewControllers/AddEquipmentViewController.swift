//
//  AddEquipmentParentView.swift
//  Coolex
//
//  Created by Tayseer on 8/24/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import DropDown
class AddEquipmentViewController: UIViewController {
    //MARK:- IBoutlet

    /*

     102-FG-480174
     102-FG-480173
     102-FG-480172
     102-FG-480171
     102-FG-480170
 */
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    //brand and modul view
    @IBOutlet weak var brandAndModulView: UIView!
    @IBOutlet weak var searchBrandTextField: SearchTextField!
    @IBOutlet weak var searchModuleTextField: SearchTextField!
    @IBOutlet weak var searchSerialTextField: SearchTextField!
    

   
    //serialViewData
    @IBOutlet weak var addEquipmentBySerialView: UIView!
    @IBOutlet weak var serialNumber: UITextField!
    //add equipment view
    @IBOutlet weak var equipmentInformationView: UIView!
    @IBOutlet weak var eqipmentInformtionLable: UILabel!
    @IBOutlet weak var brandLable: UILabel!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var modulLabel: UILabel!
    @IBOutlet weak var modulName: UILabel!
    @IBOutlet weak var itemCodeLabel: UILabel!
    @IBOutlet weak var itemCodeName: UILabel!
    @IBOutlet weak var serialNumberLabel: UILabel!
    @IBOutlet weak var serialNumberName: UILabel!
    @IBOutlet weak var separatour: UILabel!
    @IBOutlet weak var voltTF: UITextField!
    @IBOutlet weak var ampTF: UITextField!
    @IBOutlet weak var sprTF: UITextField!
    @IBOutlet weak var dprTF: UITextField!
    @IBOutlet weak var roomTempTF: UITextField!
    @IBOutlet weak var ambTempTF: UITextField!
    @IBOutlet weak var sdTempTF: UITextField!
    @IBOutlet weak var rdTempTF: UITextField!
    @IBOutlet weak var selectGroupButton: UIButton!
    @IBOutlet weak var selectActourButton: UIButton!
    @IBOutlet weak var selectFaulitButton: UIButton!

    @IBOutlet weak var contractItemCDButton: UIButton!
    @IBOutlet weak var contractSerialNumberButton: UIButton!
    @IBOutlet weak var contractContainerView:UIView!
     let contractItemDropDown = DropDown()
     let contractSerialDropDown = DropDown()

    var contractItems:[ContractItem] = []
    var contractSerials:[ContractSerial] = []

    var selectedContractItem:ContractItem?
    var selectedContractSerials:ContractSerial?
    //MARK:- Variable
    var woDetails:WorkOrderDetailsResponse?
    var brands = [Brand]()
    let brandDropDown = DropDown()
    var isBrandDropDownShow = false
    var moduls = [equipmentModul]()
    let modulDropDown = DropDown()
    var brandModuleSerials =   [EqupimentSearchResult]();
    let serialDropDown = DropDown()
    var isModulDropDownShow = false
     var isSerialDropDownShow = false
    var groups = [Group]()
    let groupDropDown = DropDown()
    var isGroupDropDownShow = false
    var faults = [Fault]()
    let faultDropDown = DropDown()
    var isFaultDropDownShow = false
    var actionFault = [ActionFault]()
    let actionDropDown = DropDown()
    var isActionDropDownShow = false
    var selectedModul: equipmentModul?;
    var selectedBrand: Brand?;
    var selectedGroup: Group?;
    var selectedFault: Fault?;
    var selectedActionFault: ActionFault?;
    var equpimentSearchResult: EqupimentSearchResult?;
    //MARK:- viewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView();
        configBrandSearchText()
        configModuleSearchText()
        configSerialSearchText()

        guard let type = woDetails?.response?.type else {return}
        if (type.uppercased() !=  "N" ){
            contractContainerView.isHidden = false
            segmentControl.isHidden = true
            getContractUnits()
        }else {
            contractContainerView.isHidden = true
            segmentControl.isHidden = false
        }
    }


    func getContractUnits (){

        guard let contractcd = woDetails?.response?.contractno else { return  }
            SVProgressHUD.show()
            provider.request(.getContractUnits(contractcd: contractcd)) { (result) in
                SVProgressHUD.dismiss()
                switch result{
                case let .success(moyaResponse):
                    let json = try! JSON(data:moyaResponse.data)
                    let statusCode = json["status"]["statuscode"].intValue
                    switch statusCode
                    {
                    case 0 :
                        //SUCCESS

                        if let groupsJsonArray = json["response"]["Units"].arrayObject as? [[String : Any]] {
                            self.contractItems = Mapper<ContractItem>().mapArray(JSONArray: groupsJsonArray)
                            self.setContractItemDropDown()


                        }
                        break;
                    case 1:
                        //FAILED
                        break;
                    case 404:
                        //UNAUTHORIZED
                        break;
                    case 800:
                        //technical issue
                        break;
                    default:
                        break

                    }
                case let .failure(error):
                    print(error)
                }

            }


    }

    func setContractItemDropDown()
    {

        contractItemDropDown.dataSource = self.contractItems.map({ (contract) -> String in
            return contract.itmCd!
        })

            // The view to which the drop down will appear on
            contractItemDropDown.anchorView = contractItemCDButton // UIView or UIBarButtonItem
            contractItemDropDown.selectionAction = {(index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                print("Drop down dismissed")
                //select and dismiss
                self.contractItemCDButton.setTitle(item, for: .normal)

                self.selectedContractItem = self.contractItems[index]
                self.contractSerials = []
                self.selectedContractSerials = nil
                self.contractSerialNumberButton.setTitle("Select Serial", for: .normal)
                self.clearForBrandSelection()
                self.getContractDevices()

            }
            contractItemDropDown.cancelAction = {
                print("Drop down dismissed")

            }




    }

    func getContractDevices (){

        guard let contractcd = woDetails?.response?.contractno else { return  }
        guard let itemCD = self.selectedContractItem?.itmCd else {return}

        SVProgressHUD.show()
        provider.request(.getContractDevices(contractcd: contractcd, itm_cd: itemCD)    ) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS

                    if let groupsJsonArray = json["response"]["Devices"].arrayObject as? [[String : Any]] {
                        self.contractSerials = Mapper<ContractSerial>().mapArray(JSONArray: groupsJsonArray)
                        self.setContractSerialDropDown()


                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break

                }
            case let .failure(error):
                print(error)
            }

        }

    }

    func setContractSerialDropDown()
    {

        contractSerialDropDown.dataSource = self.contractSerials.map({ (serial) -> String in
            return serial.serialnumber!
        })

        // The view to which the drop down will appear on
        contractSerialDropDown.anchorView = contractSerialNumberButton// UIView or UIBarButtonItem
        contractSerialDropDown.selectionAction = {(index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print("Drop down dismissed")
            //select and dismiss
            self.contractSerialNumberButton.setTitle(item, for: .normal)

            self.selectedContractSerials = self.contractSerials[index]
            self.getBySerialNumber(serialNumber: item)
        }
        contractSerialDropDown.cancelAction = {
            print("Drop down dismissed")

        }

    }

    @IBAction func ContractItemsTapped(_ sender: Any) {
        contractItemDropDown.show();
    }
    @IBAction func ContractSerialTapped(_ sender: Any) {
        if (contractSerialDropDown.dataSource.count > 0){
        contractSerialDropDown.show();}
        else {
     contractItemDropDown.show();
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3449808884, green: 0.8360603401, blue: 1, alpha: 1)
    }
    //MARK:- Helping Function
    func setupView(){
        displayBrandList();

        selectGroupButton.isUserInteractionEnabled = false
        if let groupcd = woDetails?.response?.groupcd
        {
            selectGroupButton.setTitle(groupcd, for: .normal)
        }
        //fakeBrand()
    }
    func displayBrandList(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            // Here you will get the animation you want
            self.brandAndModulView.alpha = 1
            self.addEquipmentBySerialView.alpha = 0
        }, completion: { _ in
            // Here you hide it when animation done
            self.brandAndModulView.isHidden = false
            self.addEquipmentBySerialView.isHidden = true
        })
    }
    func displaySerialView(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            // Here you will get the animation you want
            self.brandAndModulView.alpha = 0
            self.addEquipmentBySerialView.alpha = 1
        }, completion: { _ in
            // Here you hide it when animation done
            self.brandAndModulView.isHidden = true
            self.addEquipmentBySerialView.isHidden = false
        })

    }





    func clearForSerialSelection () {
//        self.serialButton.setTitle("Select Serial", for: .normal)
//        self.serialNumberName.text = ""
//        self.itemCodeName.text = ""
    }

//    func setModulDropDownList(modulName : [equipmentModul])
//    {
//        //init for first time
//        if modulDropDown.dataSource.count == 0 {
//        // The view to which the drop down will appear on
//        modulDropDown.anchorView = modulButton // UIView or UIBarButtonItem
//        modulDropDown.selectionAction = {(index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            print("Drop down dismissed")
//            self.clearForModuleSelection()
//            //select and dismiss
//            self.modulButton.setTitle(item, for: .normal)
//            self.isModulDropDownShow = false
//            self.selectedModul = self.moduls[index]
//            self.searchByModulAndbrand()
//        }
//        modulDropDown.cancelAction = {
//            print("Drop down dismissed")
//            self.isModulDropDownShow = false
//        }
//
//        modulDropDown.willShowAction = {
//            print("Drop down will show")
//            self.isModulDropDownShow = true
//        }
//        }
//        // The list of items to display. Can be changed dynamically
//        var modulNameArrary = [String]()
//        for modul in modulName{
//            modulNameArrary.append("\(modul.modulename!) - \(modul.modulecd!)")
//        }
//        modulDropDown.dataSource = modulNameArrary
//    }

//    func setSerialDropDownList(moduleSerials : [EqupimentSearchResult])
//    {
//        //init for first time
//        if serialDropDown.dataSource.count == 0 {
//            // The view to which the drop down will appear on
//            serialDropDown.anchorView = modulButton // UIView or UIBarButtonItem
//            serialDropDown.selectionAction = {(index: Int, item: String) in
//                print("Selected item: \(item) at index: \(index)")
//                print("Drop down dismissed")
//                self.clearForSerialSelection()
//                //select and dismiss
//                self.serialButton.setTitle(item, for: .normal)
//                self.isSerialDropDownShow = false
//                self.equpimentSearchResult = self.brandModuleSerials[index]
//                self.getBySerialNumber(serialNumber:  (self.equpimentSearchResult?.serialnumber!)!)
//               // self.searchByModulAndbrand()
//
//                self.getGroups()
//                self.getFaults()
//                //                        self.fullEquipmentInfomationData(data : self.equpimentSearchResult!);
//
//            }
//            modulDropDown.cancelAction = {
//                print("Drop down dismissed")
//                self.isSerialDropDownShow = false
//                 self.serialButton.setTitle("Serial", for: .normal)
//            }
//
//            modulDropDown.willShowAction = {
//                print("Drop down will show")
//                self.isSerialDropDownShow = true
//            }
//        }
//        // The list of items to display. Can be changed dynamically
//        var serialNameArrary = [String]()
//        for modul in moduleSerials{
//            serialNameArrary.append("\(modul.serialnumber!)")
//        }
//        serialDropDown.dataSource = serialNameArrary
//    }
    func setGroupDropDownList(groupName : [Group])
    {
        //init for first time
        if groupDropDown.dataSource.count == 0 {
            // The view to which the drop down will appear on
            groupDropDown.anchorView = selectGroupButton // UIView or UIBarButtonItem
            groupDropDown.selectionAction = {(index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                print("Drop down dismissed")
                //select and dismiss
                self.selectGroupButton.setTitle(item, for: .normal)
                self.isGroupDropDownShow = false
                self.selectedGroup = self.groups[index]
            }
            groupDropDown.cancelAction = {
                print("Drop down dismissed")
                self.isGroupDropDownShow = false
            }
            
            groupDropDown.willShowAction = {
                print("Drop down will show")
                self.isGroupDropDownShow = true
            }
        }
        // The list of items to display. Can be changed dynamically
        var groupNameArrary = [String]()
        for group in groupName{
            groupNameArrary.append(group.groupdesc!)
        }
        groupDropDown.dataSource = groupNameArrary
    }
    func setFaultDropDownList(faultName : [Fault])
    {
        //init for first time
        if faultDropDown.dataSource.count == 0 {
            // The view to which the drop down will appear on
            faultDropDown.anchorView = selectFaulitButton // UIView or UIBarButtonItem
            faultDropDown.selectionAction = {(index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                print("Drop down dismissed")
                //select and dismiss
                self.selectFaulitButton.setTitle(item, for: .normal)
                self.isFaultDropDownShow = false
                self.selectedFault = self.faults[index]
                self.getFaultAction()
            }
            faultDropDown.cancelAction = {
                print("Drop down dismissed")
                self.isFaultDropDownShow = false
            }
            
            faultDropDown.willShowAction = {
                print("Drop down will show")
                self.isFaultDropDownShow = true
            }
        }
        // The list of items to display. Can be changed dynamically
        var faultNameArrary = [String]()
        for fault in faultName{
            faultNameArrary.append(fault.faultdesc!)
        }
        faultDropDown.dataSource = faultNameArrary
    }
    func setActionFaultDropDownList(actionFaultName : [ActionFault])
    {
        //init for first time
        if actionDropDown.dataSource.count == 0 {
            // The view to which the drop down will appear on
            actionDropDown.anchorView = selectActourButton // UIView or UIBarButtonItem
            actionDropDown.selectionAction = {(index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                print("Drop down dismissed")
                //select and dismiss
                self.selectActourButton.setTitle(item, for: .normal)
                self.isActionDropDownShow = false
                self.selectedActionFault = self.actionFault[index]
            }
            actionDropDown.cancelAction = {
                print("Drop down dismissed")
                self.isActionDropDownShow = false
            }
            
            actionDropDown.willShowAction = {
                print("Drop down will show")
                self.isActionDropDownShow = true
            }
        }
        // The list of items to display. Can be changed dynamically
        var actionfaultNameArrary = [String]()
        for actionFault in actionFaultName{
            actionfaultNameArrary.append(actionFault.actiondesc!)
        }
        actionDropDown.dataSource = actionfaultNameArrary
    }



    func fullEquipmentInfomationData(data : EqupimentSearchResult)
    {
        brandName.text = data.brand
        modulName.text = data.module
        itemCodeName.text = data.itemcode
        serialNumberName.text = data.serialnumber
    }

    //MARK:- API calls
//    func getBrands()  {
//        SVProgressHUD.show()
//        provider.request(.GetAllBrand()) { (result) in
//            SVProgressHUD.dismiss()
//            switch result{
//            case let .success(moyaResponse):
//                let json = try! JSON(data:moyaResponse.data)
//                let statusCode = json["status"]["statuscode"].intValue
//                switch statusCode
//                {
//                case 0 :
//                    //SUCCESS
//
//                    if let brandsJsonArray = json["response"]["brands"].arrayObject as? [[String : Any]] {
//                        self.brands = Mapper<Brand>().mapArray(JSONArray: brandsJsonArray)
//                        self.setBrandDropDownList(brandName: self.brands)
//                        if (self.brands.count == 0)
//                        {
//                            //self.fakeBrand()
//                        }
//                        //self.brandListTableView.reloadData()
//                    }
//                    break;
//                case 1:
//                    //FAILED
//                    break;
//                case 404:
//                    //UNAUTHORIZED
//                    break;
//                case 800:
//                    //technical issue
//                    break;
//                default:
//                    break
//
//                }
//            case let .failure(error):
//                print(error)
//            }
//
//        }
//    }
//    func getModul(brand: Brand)  {
//        SVProgressHUD.show()
//        provider.request(.GetModules(brandcd:brand.brandcd!)) { (result) in
//            SVProgressHUD.dismiss()
//            switch result{
//            case let .success(moyaResponse):
//                let json = try! JSON(data:moyaResponse.data)
//                let statusCode = json["status"]["statuscode"].intValue
//                switch statusCode
//                {
//                case 0 :
//                    //SUCCESS
//
//                    if let modulesJsonArray = json["response"]["modules"].arrayObject as? [[String : Any]] {
//                        self.moduls = Mapper<equipmentModul>().mapArray(JSONArray: modulesJsonArray)
//                        self.setModulDropDownList(modulName: self.moduls)
//                        if (self.moduls.count == 0)
//                        {
//                            //self.fakeModul()
//                            // create the alert
//                            let alert = UIAlertController(title: "Alert", message: "No Modul for hs band now.", preferredStyle: UIAlertControllerStyle.alert)
//                            // add an action (button)
//                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                            // show the alert
//                            self.present(alert, animated: true, completion: nil)
//                        }
//                        //self.brandListTableView.reloadData()
//                    }
//                    break;
//                case 1:
//                    //FAILED
//                    break;
//                case 404:
//                    //UNAUTHORIZED
//                    break;
//                case 800:
//                    //technical issue
//                    break;
//                default:
//                    break
//
//                }
//            case let .failure(error):
//                print(error)
//            }
//
//        }
//    }
    func getGroups()  {
        SVProgressHUD.show()
        provider.request(.GetGroup()) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    
                    if let groupsJsonArray = json["response"]["groups"].arrayObject as? [[String : Any]] {
                        self.groups = Mapper<Group>().mapArray(JSONArray: groupsJsonArray)
                        self.setGroupDropDownList(groupName: self.groups)
                        
                        
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
    }
    func getFaults()  {
        SVProgressHUD.show()
        provider.request(.GetFaults()) { (result) in
                        SVProgressHUD.dismiss()
                        switch result{
                        case let .success(moyaResponse):
                            let json = try! JSON(data:moyaResponse.data)
                            let statusCode = json["status"]["statuscode"].intValue
                            switch statusCode
                            {
                            case 0 :
                                //SUCCESS

                                if let faultsJsonArray = json["response"]["faults"].arrayObject as? [[String : Any]] {
                                    self.faults = Mapper<Fault>().mapArray(JSONArray: faultsJsonArray)
                                    self.setFaultDropDownList(faultName: self.faults)
                                }
                                break;
                            case 1:
                                //FAILED
                                break;
                            case 404:
                                //UNAUTHORIZED
                                break;
                            case 800:
                                //technical issue
                                break;
                            default:
                                break

                            }
                        case let .failure(error):
                            print(error)
                        }

        }

    }
    func getFaultAction()  {
        SVProgressHUD.show()
        provider.request(.GetActionFaults(faultcode: (selectedFault?.faultcode)!)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    
                    if let faultsJsonArray = json["response"]["actionfaults"].arrayObject as? [[String : Any]] {
                        self.actionFault = Mapper<ActionFault>().mapArray(JSONArray: faultsJsonArray)
                        self.setActionFaultDropDownList(actionFaultName: self.actionFault)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
    }

        func getBySerialNumber(serialNumber:String) {
        SVProgressHUD.show()
        provider.request(.GetBySerialNumber(serialNumber: serialNumber)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    self.equpimentSearchResult = Mapper<EqupimentSearchResult>().map(JSONString: json["response"].rawString()!)
                    //if there was data
                    // create the alert
                    if (self.equpimentSearchResult?.brand  == nil || self.equpimentSearchResult?.module == nil)
                    {
                        //there is no serach result
                        // create the alert
                        let alert = UIAlertController(title: "Alert", message: "No Search Result.", preferredStyle: UIAlertControllerStyle.alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //self.fakeSerial(serial: serialNumber)
                    }
                    else
                    {
                        self.getGroups()
                        self.getFaults()
                        self.fullEquipmentInfomationData(data : self.equpimentSearchResult!);
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }

    }
    
    func searchByModulAndbrand() {
        SVProgressHUD.show()
        if let brand = selectedBrand?.brandname ,  let module  = selectedModul?.modulecd {
            
        
        
        provider.request(.GetByBrandandModule(brand:brand,module:module)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    self.brandModuleSerials = Mapper<EqupimentSearchResult>().mapArray(JSONString: json["response"]["serials"].rawString()!)!
                    //if there was data
                    // create the alert
                    //setSerialDropDownList
                     // if (self.equpimentSearchResult?.brand  == nil || self.equpimentSearchResult?.module == nil)
                    if ( self.brandModuleSerials.count < 1)
                    {
                        //there is no serach result
                        // create the alert
                        let alert = UIAlertController(title: "Alert", message: "No Search Result.", preferredStyle: UIAlertControllerStyle.alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //self.fakeModulAndBrandResponce(brand: self.selectedBrand!, modul: self.selectedModul!)
                    }
                    else
                    {
//                        self.getGroups()
//                        self.getFaults()
//                        self.fullEquipmentInfomationData(data : self.equpimentSearchResult!);
                        
                        //self.setSerialDropDownList(moduleSerials: self.brandModuleSerials);
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
        }
    }
    func addEqipment() {

        guard let fault = selectedFault?.faultcode else {
           self.showError(message: "Please Select Fault")
            return
        }

        guard let action = selectedActionFault?.actioncode  else {
            self.showError(message: "Please Select Action")
            return
        }

        guard let volt = voltTF.text ,  volt.count > 0 ,
            let amp = ampTF.text,  amp.count > 0 ,
            let spr = sprTF.text , spr.count > 0 ,
            let dpr = dprTF.text , dpr.count > 0 ,
            let roomTemp = roomTempTF.text ,
            let ampTemp = ambTempTF.text ,
            let sdtemp = sdTempTF.text ,
            let rdTemp = rdTempTF.text  else {
            self.showError(message: "Please Fill All Fields")
                return
        }
    if let wono = woDetails?.response?.wono ,
        let brandcd = equpimentSearchResult?.brand,
        let modul = equpimentSearchResult?.module ,
        let serial = equpimentSearchResult?.serialnumber ,
        let group = woDetails?.response?.groupcd
    {
        let paramter : [String:String] = [
                                            "wono":wono,
                                            "brandcd":brandcd,
                                            "modulecd":modul,
                                            "serialnumber":serial,
                                            "volt":volt,
                                            "amp":amp,
                                            "spr":spr,
                                            "dpr":dpr,
                                            "roomtemperature":roomTemp,
                                            "ambtemperature":ampTemp,
                                            "sducttemp":sdtemp,
                                            "rducttemp":rdTemp,
                                            "group":group,
                                            "actioncode":action,
                                            "faultcode":fault]
        SVProgressHUD.show()
        
        provider.request(.AddEquipment(dic: paramter)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    //self.fakeModul()
                    // create the alert
                    let alert = UIAlertController(title: "SUCCESS", message: "successfuly added Equipment", preferredStyle: UIAlertControllerStyle.alert)
                    // add an action (button)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                        
                        self.navigationController?.popViewController(animated: true)
                    }))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    break;
                case 1:
                    let alert = UIAlertController(title: "Error", message: "Eqipment Cannot be Added", preferredStyle: UIAlertControllerStyle.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
        }
        
    }

    func showError(message:String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
       //MARK:- IBAction
    @IBAction func searchBySerial(_ sender: Any) {
        if !(serialNumber.text?.isEmpty)! {
            getBySerialNumber(serialNumber: serialNumber.text!)
        }
        else{
            // create the alert
            let alert = UIAlertController(title: "Error", message: "plaese Enter Valid Data.", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func searchByBrandAndModulAction(_ sender: Any) {
        if (selectedBrand != nil && selectedModul != nil) {
            //getbybrandandmodul
           searchByModulAndbrand()
           
        }
        else{
            // create the alert
            let alert = UIAlertController(title: "Error", message: "plaese Choose Brand and Modul.", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func ChangeSegment(_ sender: Any) {
        
        if(segmentControl.selectedSegmentIndex==0){
            //display brand list view
            displayBrandList();
            
        }
        else if(segmentControl.selectedSegmentIndex==1){
            //display addEquimpemnt view
            displaySerialView();
        }
    }
    @IBAction func choosebrandAction(_ sender: Any) {
            brandDropDown.show();
    }
    @IBAction func chooseModulAction(_ sender: Any) {
           modulDropDown.show();
    }
    @IBAction func chooseSerialAction(_ sender: Any) {
       serialDropDown.show();
    }
    
    @IBAction func selcGoupAction(_ sender: Any) {
        groupDropDown.show()
    }
    @IBAction func selectedActionFAultAction(_ sender: Any) {
        actionDropDown.show()
    }
    @IBAction func selectFaultAction(_ sender: Any) {
        faultDropDown.show()
    }
    @IBAction func addEquipmentAction(_ sender: Any) {
        if let _ = equpimentSearchResult?.brand , let _ = equpimentSearchResult?.module , let _ = equpimentSearchResult?.serialnumber
        {
        addEqipment()
        }
        else
        {
            // create the alert
            let alert = UIAlertController(title: "Error", message: "Please Fill all Fields.", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
    }

}

// MARK:- Search Brand
extension AddEquipmentViewController {
    private func configBrandSearchText(){

        // Set a visual theme (SearchTextFieldTheme). By default it's the light theme
        searchBrandTextField.theme = SearchTextFieldTheme.lightTheme()

        // Modify current theme properties
        searchBrandTextField.theme.font = UIFont.systemFont(ofSize: 16)
        //        searchTextField.theme.bgColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 0.3)
        searchBrandTextField.theme.borderColor = UIColor.lightGray
        searchBrandTextField.theme.separatorColor = UIColor.lightGray
        searchBrandTextField.theme.cellHeight = 50
        searchBrandTextField.theme.fontColor = UIColor.darkText
        searchBrandTextField.theme.bgColor = UIColor.white
        // Set specific comparision options - Default: .caseInsensitive
        searchBrandTextField.comparisonOptions = [.caseInsensitive]

        // Set the max number of results. By default it's not limited
        searchBrandTextField.maxNumberOfResults = 10

        // You can also limit the max height of the results list
        searchBrandTextField.maxResultsListHeight = 500

        searchBrandTextField.inlineMode = false

        // Customize the way it highlights the search string. By default it bolds the string
        searchBrandTextField.highlightAttributes = [NSAttributedStringKey.backgroundColor : UIColor.yellow, NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 18)]



        // You can force the results list to support RTL languages - Default: false
        searchBrandTextField.forceRightToLeft = false

        // Show the list of results as soon as the user makes focus - Default: false
        searchBrandTextField.startVisible = false

        // ...or show the list of results even without user's interaction as soon as created - Default: false
        searchBrandTextField.startVisibleWithoutInteraction = false

        // Start filtering after an specific number of characters - Default: 0
        searchBrandTextField.minCharactersNumberToStartFiltering = 1

        // Explicitly hide the results list
        searchBrandTextField.hideResultsList()

        /**
         * Update data source when the user stops typing.
         * It's useful when you want to retrieve results from a remote server while typing
         * (but only when the user stops doing it)
         **/
        searchBrandTextField.userStoppedTypingHandler = {
            if let criteria = self.searchBrandTextField.text {
                if criteria.count >= 1 {
                    // Show the loading indicator
                    self.searchBrandTextField.showLoadingIndicator()

                    self.searchBrandItemsInBackground(criteria) { results in
                        // Set new items to filter

                        DispatchQueue.main.async {
                            self.searchBrandTextField.filterItems(results)
                            self.searchBrandTextField.stopLoadingIndicator()
                        }
                    }
                }else {
                    self.searchBrandTextField.filterItems([])
                    self.searchBrandTextField.hideResultsList()
                    self.searchBrandTextField.stopLoadingIndicator()
                }
            }
        }

        // Handle item selection - Default behaviour: item title set to the text field
        searchBrandTextField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.clearForBrandSelection()

            self.searchBrandTextField.text = item.title
            self.searchBrandTextField.stopLoadingIndicator()
            self.searchBrandTextField.resignFirstResponder()
            self.view.endEditing(true)


            self.selectedBrand = self.brands.filter({ (brand) -> Bool in
                return brand.brandname == item.title
            }).first

        }

    }


    private func searchBrandItemsInBackground(_ criteria:String , complition: @escaping (([SearchTextFieldItem]) -> Void)){

        self.searchBrandTextField.showLoadingIndicator()
        getBrandsItem(query: criteria) { (items) in
            complition(items)
        }

    }

    func getBrandsItem(query:String , completion: @escaping (([SearchTextFieldItem]) -> Void))  {
        SVProgressHUD.show()
        provider.request(.SearchBrandByName(query: query)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS

                    if let brandsJsonArray = json["response"]["brands"].arrayObject as? [[String : Any]] {
                         self.brands = Mapper<Brand>().mapArray(JSONArray: brandsJsonArray)
                        let searchBrandTextFieldItems =  self.brands.map({ (item) -> SearchTextFieldItem in
                            return SearchTextFieldItem(title: item.brandname ?? "", subtitle: item.brandcd ?? "")
                        })
                        completion(searchBrandTextFieldItems)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break

                }
            case let .failure(error):
                print(error)
            }

        }
    }

    func clearForBrandSelection () {
        self.selectedBrand = nil
        self.selectedModul = nil

        self.searchModuleTextField.text = nil
        self.searchBrandTextField.text = nil
        self.searchSerialTextField.text = nil
        self.equpimentSearchResult = nil;
        self.brandName.text = ""
        self.modulName.text = ""
        self.serialNumberName.text = ""
        self.itemCodeName.text = ""


    }
}
// MARK:- Search Module
extension AddEquipmentViewController {
    private func configModuleSearchText(){

        // Set a visual theme (SearchTextFieldTheme). By default it's the light theme
        searchModuleTextField.theme = SearchTextFieldTheme.lightTheme()

        // Modify current theme properties
        searchModuleTextField.theme.font = UIFont.systemFont(ofSize: 16)
        //        searchTextField.theme.bgColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 0.3)
        searchModuleTextField.theme.borderColor = UIColor.lightGray
        searchModuleTextField.theme.separatorColor = UIColor.lightGray
        searchModuleTextField.theme.cellHeight = 50
        searchModuleTextField.theme.fontColor = UIColor.darkText
        searchModuleTextField.theme.bgColor = UIColor.white
        // Set specific comparision options - Default: .caseInsensitive
        searchModuleTextField.comparisonOptions = [.caseInsensitive]

        // Set the max number of results. By default it's not limited
        searchModuleTextField.maxNumberOfResults = 10

        // You can also limit the max height of the results list
        searchModuleTextField.maxResultsListHeight = 500

        searchModuleTextField.inlineMode = false

        // Customize the way it highlights the search string. By default it bolds the string
        searchModuleTextField.highlightAttributes = [NSAttributedStringKey.backgroundColor : UIColor.yellow, NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 18)]



        // You can force the results list to support RTL languages - Default: false
        searchModuleTextField.forceRightToLeft = false

        // Show the list of results as soon as the user makes focus - Default: false
        searchModuleTextField.startVisible = false

        // ...or show the list of results even without user's interaction as soon as created - Default: false
        searchModuleTextField.startVisibleWithoutInteraction = false

        // Start filtering after an specific number of characters - Default: 0
        searchModuleTextField.minCharactersNumberToStartFiltering = 1

        // Explicitly hide the results list
        searchModuleTextField.hideResultsList()

        /**
         * Update data source when the user stops typing.
         * It's useful when you want to retrieve results from a remote server while typing
         * (but only when the user stops doing it)
         **/
        searchModuleTextField.userStoppedTypingHandler = {
            if let criteria = self.searchModuleTextField.text {
                if criteria.count >= 1 {
                    // Show the loading indicator
                    self.searchModuleTextField.showLoadingIndicator()

                    self.searchModuleItemsInBackground(criteria) { results in
                        // Set new items to filter

                        DispatchQueue.main.async {
                            self.searchModuleTextField.filterItems(results)
                            self.searchModuleTextField.stopLoadingIndicator()
                        }
                    }
                }else {
                    self.searchModuleTextField.filterItems([])
                    self.searchModuleTextField.hideResultsList()
                    self.searchModuleTextField.stopLoadingIndicator()
                }
            }
        }

        // Handle item selection - Default behaviour: item title set to the text field
        searchModuleTextField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.clearForModuleSelection()

            self.searchModuleTextField.text = item.title + " " + item.subtitle!
            self.searchModuleTextField.stopLoadingIndicator()
            self.searchModuleTextField.resignFirstResponder()
            self.view.endEditing(true)


            self.selectedModul = self.moduls.filter({ module -> Bool in
                return module.modulename == item.title
            }).first

        }

    }


    private func searchModuleItemsInBackground(_ criteria:String , complition: @escaping (([SearchTextFieldItem]) -> Void)){

        self.searchModuleTextField.showLoadingIndicator()
        getModulesItem(query: criteria) { (items) in
            complition(items)
        }

    }

    func getModulesItem(query:String , completion: @escaping (([SearchTextFieldItem]) -> Void))  {
        SVProgressHUD.show()
        provider.request(.SearchModuleByName(query: query)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS

                    if let modulesJsonArray = json["response"]["modules"].arrayObject as? [[String : Any]] {
                        self.moduls = Mapper<equipmentModul>().mapArray(JSONArray: modulesJsonArray)
                        let searchModulesTextFieldItems =  self.moduls.map({ (item) -> SearchTextFieldItem in
                            return SearchTextFieldItem(title: item.modulename ?? "", subtitle: item.modulecd ?? "")
                        })
                        completion(searchModulesTextFieldItems)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break

                }
            case let .failure(error):
                print(error)
            }

        }
    }

    func clearForModuleSelection () {
        self.selectedModul = nil
//        self.modulButton.setTitle("Select Module", for: .normal)
//        self.serialButton.setTitle("Select Serial", for: .normal)
        self.equpimentSearchResult = nil;
        self.modulName.text = ""
        self.serialNumberName.text = ""
        self.itemCodeName.text = ""
    }
}



// MARK:- Search serial
extension AddEquipmentViewController {
    private func configSerialSearchText(){

        // Set a visual theme (SearchTextFieldTheme). By default it's the light theme
        searchSerialTextField.theme = SearchTextFieldTheme.lightTheme()

        // Modify current theme properties
        searchSerialTextField.theme.font = UIFont.systemFont(ofSize: 16)
        //        searchTextField.theme.bgColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 0.3)
        searchSerialTextField.theme.borderColor = UIColor.lightGray
        searchSerialTextField.theme.separatorColor = UIColor.lightGray
        searchSerialTextField.theme.cellHeight = 50
        searchSerialTextField.theme.fontColor = UIColor.darkText
        searchSerialTextField.theme.bgColor = UIColor.white
        // Set specific comparision options - Default: .caseInsensitive
        searchSerialTextField.comparisonOptions = [.caseInsensitive]

        // Set the max number of results. By default it's not limited
        searchSerialTextField.maxNumberOfResults = 10

        // You can also limit the max height of the results list
        searchSerialTextField.maxResultsListHeight = 500

        searchSerialTextField.inlineMode = false

        // Customize the way it highlights the search string. By default it bolds the string
        searchSerialTextField.highlightAttributes = [NSAttributedStringKey.backgroundColor : UIColor.yellow, NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 18)]



        // You can force the results list to support RTL languages - Default: false
        searchSerialTextField.forceRightToLeft = false

        // Show the list of results as soon as the user makes focus - Default: false
        searchSerialTextField.startVisible = false

        // ...or show the list of results even without user's interaction as soon as created - Default: false
        searchSerialTextField.startVisibleWithoutInteraction = false

        // Start filtering after an specific number of characters - Default: 0
        searchSerialTextField.minCharactersNumberToStartFiltering = 1

        // Explicitly hide the results list
        searchSerialTextField.hideResultsList()

        /**
         * Update data source when the user stops typing.
         * It's useful when you want to retrieve results from a remote server while typing
         * (but only when the user stops doing it)
         **/
        searchSerialTextField.userStoppedTypingHandler = {
            if let criteria = self.searchSerialTextField.text {
                if criteria.count >= 1 {
                    // Show the loading indicator
                    self.searchSerialTextField.showLoadingIndicator()

                    self.searchSerialsItemsInBackground(criteria) { results in
                        // Set new items to filter

                        DispatchQueue.main.async {
                            self.searchSerialTextField.filterItems(results)
                            self.searchSerialTextField.stopLoadingIndicator()
                        }
                    }
                }else {
                    self.searchSerialTextField.filterItems([])
                    self.searchSerialTextField.hideResultsList()
                    self.searchSerialTextField.stopLoadingIndicator()
                }
            }
        }

        // Handle item selection - Default behaviour: item title set to the text field
        searchSerialTextField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.clearForModuleSelection()

            self.searchSerialTextField.text = item.title
            self.searchSerialTextField.stopLoadingIndicator()
            self.searchSerialTextField.resignFirstResponder()
            self.view.endEditing(true)


            self.equpimentSearchResult = self.brandModuleSerials.filter({ module -> Bool in
                return module.serialnumber == item.title
            }).first

            self.getBySerialNumber(serialNumber:  (self.equpimentSearchResult?.serialnumber)!)
        }

    }


    private func searchSerialsItemsInBackground(_ criteria:String , complition: @escaping (([SearchTextFieldItem]) -> Void)){

        self.searchSerialTextField.showLoadingIndicator()
        getSerialsItem(query: criteria) { (items) in
            complition(items)
        }

    }

    func getSerialsItem(query:String , completion: @escaping (([SearchTextFieldItem]) -> Void))  {
        SVProgressHUD.show()
        provider.request(.SearchBySerialNumber(query: query)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS

                    if let modulesJsonArray = json["response"]["itemserials"].arrayObject as? [[String : Any]] {
                        self.brandModuleSerials = Mapper<EqupimentSearchResult>().mapArray(JSONArray: modulesJsonArray)
                        let searchModulesTextFieldItems =  self.brandModuleSerials.map({ (item) -> SearchTextFieldItem in
                            return SearchTextFieldItem(title: item.serialnumber ?? "", subtitle: item.itemcode ?? "")
                        })
                        completion(searchModulesTextFieldItems)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break

                }
            case let .failure(error):
                print(error)
            }

        }
    }


}





