//
//  DemoCell.swift
//  FoldingCell
//
//  Created by Alex K. on 25/12/15.
//  Copyright © 2015 Alex K. All rights reserved.
//

import FoldingCell
import Moya_ObjectMapper
import ObjectMapper
import SVProgressHUD
import SwiftyJSON
import UIKit

class WorkOrderCell: FoldingCell {
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var collapsedContainerView: RotatedView!
    @IBOutlet weak var expandedContainerView: UIView!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var collapsBtn: UIButton!

    @IBOutlet weak var collapswopriorityLabel: UILabel!

    @IBOutlet weak var collapsTicketNoLabel: UILabel!
    @IBOutlet weak var collapsCustomerNameLabel: UILabel!
    @IBOutlet weak var collapsCustomerMobileLabel: UILabel!
    @IBOutlet weak var collapsProjectLabel: UILabel!
    @IBOutlet weak var collapsExpDateLabel: UILabel!
    @IBOutlet weak var collapsTypeLabel: UILabel!
    @IBOutlet weak var collapsAreaLabel: UILabel!
    @IBOutlet weak var collapsBlockLabel: UILabel!
    @IBOutlet weak var collapsHouseLabel: UILabel!
    @IBOutlet weak var collapsStreetLabel: UILabel!
    @IBOutlet weak var collapsAvenueLabel: UILabel!
    @IBOutlet weak var collapsFlatLabel: UILabel!
    @IBOutlet weak var collapsRemarkLabel: UILabel!
    

    
    
    @IBOutlet weak var expandComplnoLabel: UILabel!
    @IBOutlet weak var expandCustomerNameLabel: UILabel!
    @IBOutlet weak var expandProjNoLabel: UILabel!
    @IBOutlet weak var expandAreaLabel: UILabel!
    @IBOutlet weak var expandStatus: UILabel!
    @IBOutlet weak var expandStatusAction: UILabel!
    @IBOutlet weak var expandCustomerMobileLabel: UILabel!

    @IBOutlet weak var expandStreetNameLabel: UILabel!
    @IBOutlet weak var expandBlockLabel: UILabel!
    @IBOutlet weak var expandAddressNameLabel: UILabel!
    @IBOutlet weak var expandHouseNoLabel: UILabel!

    var cellColor:UIColor = .gray{
        didSet{
            leftView.backgroundColor = cellColor

        }
    }
    
    var workorder:Workorder?
    var woDetails:WorkOrderDetailsResponse?
    var parentNavigationController:UINavigationController?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func animationDuration(_ itemIndex: NSInteger, type: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
    @IBAction func orAction(_ sender:UIButton){
        
        
    }
    
    
    @IBAction func buttonHandler(_ sender: AnyObject) {
        print("tap")
    }
    
    @IBAction func presentDetails(_ sender: Any) {
        
        let stb = UIStoryboard(name: "Main", bundle: nil);
        
        let details = stb.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
        details.woDetails = self.woDetails
        
        
        self.parentNavigationController?.pushViewController(details, animated: true)
        
        
        
    }
    func displayWorkORDERDETAILS(){

       





        expandAddressNameLabel.text = self.woDetails?.response?.addresscivilid
        expandAreaLabel.text = self.woDetails?.response?.area
        expandBlockLabel.text = self.woDetails?.response?.block
        expandCustomerMobileLabel.text = self.woDetails?.response?.mobile
        expandCustomerNameLabel.text = self.woDetails?.response?.customername
        expandHouseNoLabel.text = self.woDetails?.response?.houseno
        expandStatus.text = self.woDetails?.response?.type
        expandStatusAction.text = self.woDetails?.response?.wostatusaction?.statusactionname
        expandStreetNameLabel.text = self.woDetails?.response?.street

    }
    
    func getWorkOrdersDetails() {
        guard let woid = self.workorder?.woid else { return  }
        SVProgressHUD.show()
        provider.request(.GetWorkOrderDetails(woid: woid)) { (response) in
            SVProgressHUD.dismiss()
            switch response{
            case let .success(moyaResponse):
                do {
                    self.woDetails = try moyaResponse.mapObject(WorkOrderDetailsResponse.self)
                    
                    self.displayWorkORDERDETAILS()
                    
                }catch let error  {
                    
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
}
