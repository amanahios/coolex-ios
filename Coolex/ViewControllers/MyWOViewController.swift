//
//  MyWOViewController.swift
//  Coolex
//
//  Created by Bassem on 3/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import UIKit
import Moya
import SVProgressHUD
import SwiftyJSON

class MyWOViewController: UIViewController {
    
    @IBOutlet weak var woTableView: UITableView!
    var workOrders = Array<Workorder>()
    
    override func viewDidLoad() {
        
        self.getWorkOrders()
    }
    
    func getWorkOrders() {
        SVProgressHUD.show()
        provider.request(.GetWorkOrdersByGroup(groupcd: UserDefaults.standard.value(forKey: groupcdUserDefaults) as! String)) { (response) in
            SVProgressHUD.dismiss()
            switch response{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                print(json)
                for index in 0 ..< json["response"]["workorders"].count {
                    //self.workOrders.append(Workorder(map: json["response"]["workorders"][index]))
                }
                self.woTableView.reloadData()
                
                
            case let .failure(error):
                print(error)
            }
        }
    }
    
}


extension MyWOViewController: UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.workOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MyWOTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyWOTableViewCell") as! MyWOTableViewCell
        
        let workOrder = self.workOrders[indexPath.row] 
        
        cell.workOrderName.text = workOrder.complno
        cell.workOrderId.text = workOrder.woid
        // Removed Prop
        assert(true)
        print(workOrder.toJSONString(prettyPrint: true))
//        cell.status.text = workOrder.wostatus as? String
        cell.dueData.text = workOrder.manualwono as? String
        
        return cell;
        
    }
    
    
}
