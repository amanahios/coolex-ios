//
//  equipmentTableViewCell.swift
//  Coolex
//
//  Created by Bassem on 10/17/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit

class equipmentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var moduleLabel: UILabel!
    
    @IBOutlet weak var serialLabel: UILabel!
    
    @IBOutlet weak var sprLabel: UILabel!
    @IBOutlet weak var ampLabel: UILabel!
    @IBOutlet weak var voltLabel: UILabel!
    @IBOutlet weak var dprLabel: UILabel!
    
    @IBOutlet weak var roomtempLabel: UILabel!
    @IBOutlet weak var ambtemp: UILabel!
    
    @IBOutlet weak var sduLabel: UILabel!
    @IBOutlet weak var rduLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    
    @IBOutlet weak var faultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
