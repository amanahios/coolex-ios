//
//  LoginViewController.swift
//  Coolex
//
//  Created by Amr AbdElAal on 5/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper

//MARK: Public Variables
let groupcdUserDefaults = "groupcd"


func showMessage(title:String,message:String,buttons:Array<Any>,action:(_ buttonIndex:NSInteger)->()) {
    
//    let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    
    
}

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        #if DEBUG
//       username.text = "g1";
//       password.text = "123";
//        #endif
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3449808884, green: 0.8360603401, blue: 1, alpha: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        SVProgressHUD.show()
        provider.request(.Login(email: self.username.text!, password: self.password.text!)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
                
            case let .success(moyaResponse):
                
                let json = try! JSON(data:moyaResponse.data)
                
                let group = Mapper<Group>().map(JSONString: json["response"]["group"].rawString()!)
                //map(JSONString: json["response"]["group"].dictionary)
                //let group = Group(fromJson: json["response"]["group"])
                print(json)
                print(group?.employeename ?? "")
                
                if(json["status"]["statusdescription"] == "SUCCESS"){
                    
                    UserDefaults.standard.set(group?.groupcd, forKey: groupcdUserDefaults)
                    UserDefaults.standard.synchronize()
                    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let navigationController = UINavigationController(rootViewController: home)
                    appDelegate.window?.rootViewController = navigationController
                    appDelegate.window?.makeKeyAndVisible();
                } else {
                    
                }
                
                
            case let .failure(error):
                print(error)
            }
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
