//
//  EditEquipment.swift
//  Coolex
//
//  Created by germainItLab on 8/28/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import DropDown
class UpdateWOStatusViewController: UIViewController {
    //MARK:- IBOutLet
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var statusActionButton: UIButton!
    @IBOutlet weak var techremarkTextField: UITextView!
    @IBOutlet weak var customerremarkTextField: UITextView!
    @IBOutlet weak var updateBtn: UIButton!
    //MARK:- variable
    var isClosed = false
    var woDetails:WorkOrderDetailsResponse?
    var type = ""
    var lov = [LovModul]()
    let lovDropDown = DropDown()
    var isLovDropDownShow = false
    var selectedLov: LovModul?;
    var actionStatus = [Wostatusaction]()
    let actionStatusDropDown = DropDown()
    var isActionStatusDropDownShow = false
    var selectedActionStatus: Wostatusaction?;
    //MARK:- viewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView();
        
        if (isClosed == true){
            self.updateBtn.removeFromSuperview()
            self.statusButton.removeFromSuperview()
            self.statusActionButton.removeFromSuperview()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3449808884, green: 0.8360603401, blue: 1, alpha: 1)
    }
    //MARK:- Helping Function
    func setupView(){
        getStatues(type:"WOSTATUSTYPELOV")
    }
    func setLovDropDownList(lovName : [LovModul])
    {
        // The view to which the drop down will appear on
        lovDropDown.anchorView = statusButton // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        var lovNameArrary = [String]()
        for lov in lovName{
            lovNameArrary.append(lov.name!)
        }
        lovDropDown.dataSource = lovNameArrary
        lovDropDown.selectionAction = {(index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print("Drop down dismissed")
            //select and dismiss
            self.statusButton.setTitle(item, for: .normal)
            self.isLovDropDownShow = false
            self.selectedLov = self.lov[index];
            self.getStatuesByAction(wostatuslovid: (self.selectedLov?.lovid!)!)
        }
        lovDropDown.cancelAction = {
            print("Drop down dismissed")
            self.isLovDropDownShow = false
        }
        lovDropDown.willShowAction = {
            print("Drop down will show")
            self.isLovDropDownShow = true
        }
    }
    
    func setActionsDropDownList(lovName : [Wostatusaction])
    {
        // The view to which the drop down will appear on
        actionStatusDropDown.anchorView = statusActionButton // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        var lovNameArrary = [String]()
        for lov in lovName{
            lovNameArrary.append(lov.statusactionname!)
        }
        actionStatusDropDown.dataSource = lovNameArrary
        actionStatusDropDown.selectionAction = {(index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print("Drop down dismissed")
            //select and dismiss
            self.statusActionButton.setTitle(item, for: .normal)
            self.isActionStatusDropDownShow = false
            self.selectedActionStatus = self.actionStatus[index];
        }
        actionStatusDropDown.cancelAction = {
            print("Drop down dismissed")
            self.isActionStatusDropDownShow = false
        }
        actionStatusDropDown.willShowAction = {
            print("Drop down will show")
            self.isActionStatusDropDownShow = true
        }
    }

    
    //MARK:- API calls
    func getStatues(type : String)  {
        SVProgressHUD.show()
        provider.request(.getStatus(type:type)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    if let lovJsonArray = json["response"]["lovs"].arrayObject as? [[String : Any]] {
                        self.lov = Mapper<LovModul>().mapArray(JSONArray: lovJsonArray)
                        self.setLovDropDownList(lovName: self.lov)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    func getStatuesByAction(wostatuslovid : String)  {
        SVProgressHUD.show()
        provider.request(.GetWorkOrderStatusAction(wostatuslovid: wostatuslovid)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["wostatusactions"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    if let lovJsonArray = json["response"]["wostatusactions"].arrayObject as? [[String : Any]] {
                        self.actionStatus = Mapper<Wostatusaction>().mapArray(JSONArray: lovJsonArray)
                        self.setActionsDropDownList(lovName: self.actionStatus)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
    }
    @IBAction func status(_ sender: Any) {
        actionStatus  = [];
        lovDropDown.show()
        self.statusActionButton.setTitle("Choose", for: .normal)

        
    }
    @IBAction func statusAction(_ sender: Any) {
        actionStatusDropDown.show()
    }
    @IBAction func update(_ sender: Any) {
        if let customerMark = customerremarkTextField.text , let techremark = techremarkTextField.text , let wono = self.woDetails?.response?.woid , let wostatusactionid = selectedActionStatus?.wostatusactionid , let wostatusid = selectedLov?.lovid
        {
            let paramter = ["woid":wono,
                            "wostatusid":wostatusid,
                            "wostatusactionid":wostatusactionid,
                            "techremark":techremark,
                            "customerremark":customerMark]
            SVProgressHUD.show()
            provider.request(.UpdateWorkOrderDetails(paramter)) { (result) in
                SVProgressHUD.dismiss()
                switch result{
                case let .success(moyaResponse):
                    let json = try! JSON(data:moyaResponse.data)
                    let statusCode = json["status"]["wostatusactions"].intValue
                    switch statusCode
                    {
                    case 0 :
                        //SUCCESS
                        let alert = UIAlertController(title: "SUCCESS", message: "successfuly order Updated", preferredStyle: UIAlertControllerStyle.alert)
                        // add an action (button)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                            
                            self.navigationController?.popViewController(animated: true)
                        }))
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        break;
                    case 1:
                        //FAILED
                        
                        let alert = UIAlertController(title: "FAILED", message: "FAILED to update order.", preferredStyle: UIAlertControllerStyle.alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        break;
                    case 404:
                        //UNAUTHORIZED
                        break;
                    case 800:
                        //technical issue
                        break;
                    default:
                        break
                        
                    }
                case let .failure(error):
                    print(error)
                }
                
            }

            
        }
        
        
        
    }

}
