//
//  AddUsedItemViewController.swift
//  Coolex
//
//  Created by Bassem Abbas on 8/27/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//
import Foundation
import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import DropDown

class AddUsedItemViewController: UIViewController {

    
    
     @IBOutlet weak var uomTextfield: UITextField!
    @IBOutlet weak var priceTextfield: UITextField!
    
    @IBOutlet weak var quantityTextfield: UITextField!
    @IBOutlet weak var itemNameLabel: UILabel!
     @IBOutlet weak var itemCodeLabel: UILabel!
    
    @IBOutlet weak var wonoLabel: UILabel!
    @IBOutlet weak var complnoLabel: UILabel!
    @IBOutlet weak var groupcdLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    @IBOutlet weak var searchTextField: SearchTextField!
    let itemsDropDown = DropDown()
    var itemDescriptions = [ItemDescription]()
    var selecteditemDescription: ItemDescription?;
    var isitemDescriptionDropDownShow = false
    var wono:String?
    var group:String?
    var Itemcode:String?
    var woDetails:WorkOrderDetailsResponse?
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.placeholder = "search for item ex:"
        // Do any additional setup after loading the view.
        // hidden as requirment
       // wonoLabel.text = self.woDetails?.response?.woid;
        complnoLabel.text = self.woDetails?.response?.complno;
        groupcdLabel.text = self.woDetails?.response?.groupcd;
        //Removed prop
        print(self.woDetails?.toJSONString(prettyPrint: true))

        configSearchText()
    }

    private func configSearchText(){

        // Set a visual theme (SearchTextFieldTheme). By default it's the light theme
        searchTextField.theme = SearchTextFieldTheme.lightTheme()

        // Modify current theme properties
        searchTextField.theme.font = UIFont.systemFont(ofSize: 16)
        //        searchTextField.theme.bgColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 0.3)
        searchTextField.theme.borderColor = UIColor.lightGray
        searchTextField.theme.separatorColor = UIColor.lightGray
        searchTextField.theme.cellHeight = 50
        searchTextField.theme.fontColor = UIColor.darkText
        searchTextField.theme.bgColor = UIColor.white
        // Set specific comparision options - Default: .caseInsensitive
        searchTextField.comparisonOptions = [.caseInsensitive]

        // Set the max number of results. By default it's not limited
        searchTextField.maxNumberOfResults = 5

        // You can also limit the max height of the results list
        searchTextField.maxResultsListHeight = 250

        searchTextField.inlineMode = false

        // Customize the way it highlights the search string. By default it bolds the string
        searchTextField.highlightAttributes = [NSAttributedStringKey.backgroundColor : UIColor.yellow, NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 18)]



        // You can force the results list to support RTL languages - Default: false
        searchTextField.forceRightToLeft = false

        // Show the list of results as soon as the user makes focus - Default: false
        searchTextField.startVisible = false

        // ...or show the list of results even without user's interaction as soon as created - Default: false
        searchTextField.startVisibleWithoutInteraction = false

        // Start filtering after an specific number of characters - Default: 0
        searchTextField.minCharactersNumberToStartFiltering = 1

        // Explicitly hide the results list
        searchTextField.hideResultsList()

        /**
         * Update data source when the user stops typing.
         * It's useful when you want to retrieve results from a remote server while typing
         * (but only when the user stops doing it)
         **/
        searchTextField.userStoppedTypingHandler = {
            if let criteria = self.searchTextField.text {
                if criteria.count >= 1 {
                    // Show the loading indicator
                    self.searchTextField.showLoadingIndicator()

                    self.searchMoreItemsInBackground(criteria) { results in
                        // Set new items to filter

                        DispatchQueue.main.async {
                            self.searchTextField.filterItems(results)
                            self.searchTextField.stopLoadingIndicator()
                        }
                    }
                }else {
                    self.searchTextField.filterItems([])
                    self.searchTextField.hideResultsList()
                    self.searchTextField.stopLoadingIndicator()
                }
            }
        }

        // Handle item selection - Default behaviour: item title set to the text field
        searchTextField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")

            // Do whatever you want with the picked item
            self.searchTextField.text = item.title
            self.searchTextField.stopLoadingIndicator()
            self.searchTextField.resignFirstResponder()
            self.view.endEditing(true)


            self.selecteditemDescription = self.itemDescriptions.filter({ (descript) -> Bool in
                return descript.itemname == item.title
            }).first
            self.itemCodeLabel.text =  self.selecteditemDescription?.itemcode
            self.itemNameLabel.text =  self.selecteditemDescription?.itemname
            self.uomTextfield.text =  self.selecteditemDescription?.uom;
        }

    }


    private func searchMoreItemsInBackground(_ criteria:String , complition: @escaping (([SearchTextFieldItem]) -> Void)){

        self.searchTextField.showLoadingIndicator()
        getItem(query: criteria) { (items) in
            complition(items)
        }

    }

    func getItem(query:String , completion: @escaping (([SearchTextFieldItem]) -> Void))  {
        SVProgressHUD.show()
        provider.request(.GetItemDescription(query: query)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    
                    if let faultsJsonArray = json["response"]["items"].arrayObject as? [[String : Any]] {
                        self.itemDescriptions = Mapper<ItemDescription>().mapArray(JSONArray: faultsJsonArray)
                        self.setitemDropDownList(faultName: self.itemDescriptions)
                        let searchTextFieldItems = self.itemDescriptions.map({ (item) -> SearchTextFieldItem in
                            return SearchTextFieldItem(title: item.itemname!, subtitle: item.itemcode!)
                        })
                        completion(searchTextFieldItems)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
    }
    
    func setitemDropDownList(faultName : [ItemDescription])
    {
        //init for first time
        if itemsDropDown.dataSource.count == 0 {
            // The view to which the drop down will appear on
//            itemsDropDown.anchorView = selectitemsDropDownButton // UIView or UIBarButtonItem
            itemsDropDown.selectionAction = {(index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                print("Drop down dismissed")
                //select and dismiss
//                self.selectitemsDropDownButton.setTitle(self.itemDescriptions[index].itemcode, for: .normal)
                self.itemNameLabel.text = self.itemDescriptions[index].itemname
                self.itemCodeLabel.text = self.itemDescriptions[index].itemcode
                self.isitemDescriptionDropDownShow = false
                self.selecteditemDescription = self.itemDescriptions[index]
                self.uomTextfield.text =  self.selecteditemDescription?.uom;
                
              
            }
            itemsDropDown.cancelAction = {
                print("Drop down dismissed")
                self.isitemDescriptionDropDownShow = false
            }
            
            itemsDropDown.willShowAction = {
                print("Drop down will show")
                self.isitemDescriptionDropDownShow = true
            }
        }
        // The list of items to display. Can be changed dynamically
        var faultNameArrary = [String]()
        for fault in faultName{
            faultNameArrary.append(fault.itemcode!)
        }
        itemsDropDown.dataSource = faultNameArrary
    }

    
    @IBAction func chooseitemAction(_ sender: Any) {
        itemsDropDown.show();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addAction(_ sender: Any) {
        
        self.validate();
    }

    
    func validate () {
        
        var isValed = false;
        
        
        
        isValed = true;
        
        
        if (isValed) {
            // procidetoAdd item
            
            self.additemtoServer()
            
        }else {
            
            // alet validation error 
            
        }
        
    }
    
    
    
    func additemtoServer(){
//
        if   let uom = self.selecteditemDescription?.uom ,let itemcode = self.selecteditemDescription?.itemcode, let module = self.selecteditemDescription?.itemname  , let quantity = self.quantityTextfield.text {
            let price = ""
            let jsonData = ["wono": self.wono! ,
                            "group": self.group!,
                            "Itemcode":itemcode,
                            "uom":uom ,
                            "module":module ,
                            "price": price ,
                            "quantity" :quantity,
                            "unitserialno":itemcode] // to check with p
        SVProgressHUD.show()
        provider.request(.addUsedItems(jsonData)) { (result) in
            SVProgressHUD.dismiss()
            switch result{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                let statusCode = json["status"]["statuscode"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    
//                    if let brandsJsonArray = json["response"]["brands"].arrayObject as? [[String : Any]] {
//                        self.brands = Mapper<Brand>().mapArray(JSONArray: brandsJsonArray)!
//                        self.setBrandDropDownList(brandName: self.brands)
//                        //self.brandListTableView.reloadData()}
                    
                    let alert = UIAlertController(title: "SUCCESS", message: "successfuly added item", preferredStyle: UIAlertControllerStyle.alert)
                    // add an action (button)
                   
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                        
                        self.navigationController?.popViewController(animated: true)
                    }))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    
                    break;
                case 1:
                    //FAILED
                    
                    let alert = UIAlertController(title: "FAILED", message: "FAILED to add item.", preferredStyle: UIAlertControllerStyle.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }
            case let .failure(error):
                print(error)
            }
            
        }
        }
//
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
