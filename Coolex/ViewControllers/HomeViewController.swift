//
//  HomeViewController.swift
//  Coolex
//
//  Created by Amr AbdElAal on 5/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import SwiftPullToRefresh
class HomeViewController: UIViewController {
    
   //MARK:- IBOutlet
    @IBOutlet weak var loadHistoryDataButton: UIBarButtonItem!
    @IBOutlet weak var historyTableView: UITableView!
    //MARK:- variable
    var workorders = [Workorder]()
    var isOrderStatusHistory = false
    
    let kCloseCellHeight: CGFloat = 400
    let kOpenCellHeight: CGFloat = 550  
    var kRowsCount = 0
    var cellHeights: [CGFloat] = []
       var actionStatus = [Wostatusaction]()
    
    private func setup() {
        cellHeights = Array(repeating: kCloseCellHeight, count: kRowsCount)
        historyTableView.estimatedRowHeight = kCloseCellHeight
        historyTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    //MARK:- viewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        setup()
        
        historyTableView.spr_setIndicatorHeader { [weak self] in
            // do your action here
            // self?.historyTableView.spr_endRefreshing()
            self?.getWorkOrdersByGroup()
        }
        historyTableView.tableFooterView = UIView(frame: .zero)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3449808884, green: 0.8360603401, blue: 1, alpha: 1)

         getWorkOrdersByGroup()
    }
    
    //MARK:- HelpingFunction
    //Api call
    func getWorkOrdersByGroup() {
        SVProgressHUD.show()
        provider.request(.GetWorkOrdersByGroup(groupcd: UserDefaults.standard.value(forKey: groupcdUserDefaults) as! String)) { (response) in
            SVProgressHUD.dismiss()
            self.historyTableView.spr_endRefreshing()
            switch response{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                if let workordersJsonArray = json["response"]["workorders"].arrayObject as? [[String : Any]] {
                    self.workorders = Mapper<Workorder>().mapArray(JSONArray: workordersJsonArray)
                    print(self.workorders)
                    self.kRowsCount =  self.workorders.count
                    self.setup()
                    self.historyTableView.reloadData()
                    //.mapArray(JSONArray: workordersJsonArray );
                }
                else{
                    //error handler
                }
                print(json)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    func getWorkOrderStatusAction() {
        SVProgressHUD.show()
        provider.request(.GetWorkOrderStatusAction(wostatuslovid: UserDefaults.standard.value(forKey: groupcdUserDefaults) as! String)) { (response) in
            SVProgressHUD.dismiss()
            switch response{
            case let .success(moyaResponse):
                let json = try! JSON(data:moyaResponse.data)
                self.historyTableView.reloadData()
                //completion of code depend on response
                print(json)
                let statusCode = json["status"]["wostatusactions"].intValue
                switch statusCode
                {
                case 0 :
                    //SUCCESS
                    if let lovJsonArray = json["response"]["wostatusactions"].arrayObject as? [[String : Any]] {
                        self.actionStatus = Mapper<Wostatusaction>().mapArray(JSONArray: lovJsonArray)
                      //  self.setActionsDropDownList(lovName: self.actionStatus)
                    }
                    break;
                case 1:
                    //FAILED
                    break;
                case 404:
                    //UNAUTHORIZED
                    break;
                case 800:
                    //technical issue
                    break;
                default:
                    break
                    
                }

                
            case let .failure(error):
                print(error)
            }
        }
    }
    
    //MARK:- IBActions

    @IBAction func logout(_ sender: Any) {
        //delet token 
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
        if let test = UserDefaults.standard.object(forKey: groupcdUserDefaults)
        {
            print(test)
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = UINavigationController(rootViewController: login)
        appDelegate.window?.rootViewController = login
        appDelegate.window?.makeKeyAndVisible();
//       self.navigationController!.popToRootViewController(animated: true)

    }
    @IBAction func loadHistoryData(_ sender: Any) {
       
        let historyViewController = self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
       
        
            self.navigationController?.pushViewController(historyViewController, animated: false)
        
        
    }
}
extension HomeViewController : UITableViewDelegate , UITableViewDataSource
{

    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  kRowsCount
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as WorkOrderCell = cell else {
            return
        }
        
        cell.backgroundColor = .clear
        
        if cellHeights[indexPath.row] == kCloseCellHeight {
//            cell.selectedAnimation(false, animated: false, completion:nil)
        } else {
//            cell.selectedAnimation(true, animated: false, completion: nil)
        }
        
        
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkOrderCell", for: indexPath) as! WorkOrderCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
    
//        cell.durationsForExpandedState = durations
//        cell.durationsForCollapsedState = durations
        //cell.expandBtn.tag = indexPath.row;
        //cell.collapsBtn.tag = indexPath.row
        let wo = workorders[indexPath.row] ;
        
        cell.collapswopriorityLabel.text = wo.woid
        cell.workorder = wo;
        cell.collapsTicketNoLabel.text = wo.complno
        cell.collapsCustomerNameLabel.text =  wo.customerInfo?.customername
        cell.collapsCustomerMobileLabel.text =   wo.customerInfo?.mobile
        cell.collapsProjectLabel.text =   wo.contractInfo?.projectNo
        cell.collapsExpDateLabel.text =    wo.contractInfo?.expirationDate
        cell.collapsTypeLabel.text =   wo.type
        cell.collapsAreaLabel.text = wo.orderAddress?.area
        cell.collapsBlockLabel.text = wo.orderAddress?.block
        cell.collapsHouseLabel.text = wo.orderAddress?.houseno
        cell.collapsStreetLabel.text = wo.orderAddress?.street
        cell.collapsAvenueLabel.text = wo.orderAddress?.avenue
        cell.collapsFlatLabel.text =  "-"
        cell.collapsRemarkLabel.text = wo.techremark
        
        if let  wostatus = wo.wostatusaction?.statusactionname{
            switch wostatus.lowercased() {
            case "open":
                cell.cellColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                break;
            case "close":
                cell.cellColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
              break;
             case "reschedule":
                cell.cellColor = #colorLiteral(red: 0.3647058824, green: 0.2941176471, blue: 0.6, alpha: 1)
                break;

            case "in progress":
                cell.cellColor =  #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
                break;
            default:
                cell.cellColor = #colorLiteral(red: 0.03590759635, green: 0.1280611157, blue: 0.2772453129, alpha: 1)
            }

        }
        
        cell.parentNavigationController = self.navigationController;
        cell.collapsedContainerView.layer.masksToBounds = false
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let cell = tableView.cellForRow(at: indexPath) as! WorkOrderCell


        let details = self.storyboard?.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
        details.woid = workorders[indexPath.row].woid!
        details.cellColor = cell.cellColor
        if !details.woid.isEmpty {
            self.navigationController?.pushViewController(details, animated: true)
        }

    }
    
    
    @IBAction func expandCell (sender: UIButton) {

        let indexPath = IndexPath(item: sender.tag, section: 0)
        let cell = historyTableView.cellForRow(at: indexPath) as! WorkOrderCell

        if cell.isAnimating() {
            return
        }

        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            cell.getWorkOrdersDetails()
            cellHeights[indexPath.row] = kOpenCellHeight
//            cell.selectedAnimation(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = kCloseCellHeight
//            cell.selectedAnimation(false, animated: true, completion: nil)
            duration = 0.8
        }

        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            self.historyTableView.beginUpdates()
            self.historyTableView.endUpdates()
        }, completion: nil)



    }

    
    @IBAction func collapsCell (sender:UIButton){
        
        let indexPath = IndexPath(item: sender.tag, section: 0);
        let cell = historyTableView.cellForRow(at: indexPath) as! WorkOrderCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == kCloseCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = kOpenCellHeight
//            cell.selectedAnimation(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = kCloseCellHeight
//            cell.selectedAnimation(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            self.historyTableView.beginUpdates()
            self.historyTableView.endUpdates()
        }, completion: nil)
        
        
        
    }


}





