//
//  ListDetailsTableViewController.swift
//  Coolex
//
//  Created by Bassem on 10/17/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit
import SwiftPullToRefresh
import SVProgressHUD

class ListDetailsTableViewController: UIViewController  , UITableViewDelegate, UITableViewDataSource{

    var type = "item";
    var woDetails :WorkOrderDetailsResponse?
    var woid : String?;
    @IBOutlet weak var listTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.woid = woDetails?.response?.woid;
        // Do any additional setup after loading the view.
        
        listTableView.spr_setIndicatorHeader { [weak self] in
            // do your action here
            // self?.historyTableView.spr_endRefreshing()
            self?.getHistoryDetails()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "item" {
            
            return  woDetails?.response?.items?.count ?? 0
        }else {
            
             return  woDetails?.response?.equipment?.count ?? 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell
        if type == "item" {
            
           var  itemcell  =   tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as?  itemTableViewCell
            
            if let items = self.woDetails?.response?.items  {
                 let item = items[indexPath.row]
               // itemcell?.itemcodeLabel.text = item.itemcode
                itemcell?.uomLabel.text = item.uom
                itemcell?.moduleLabel.text = item.module
                itemcell?.priceLabel.text = item.price
                itemcell?.quantityLabel.text = item.quantity
                
            }
            return itemcell!;
            
        }else {
            
            var  itemcell  =   tableView.dequeueReusableCell(withIdentifier: "equipment", for: indexPath) as?  equipmentTableViewCell
            
            if let items = self.woDetails?.response?.equipment  {
                let item = items[indexPath.row]
                
                itemcell?.brandLabel.text = item.brandcd
                itemcell?.moduleLabel.text = item.modulecd
                itemcell?.serialLabel.text = item.serialnumber
                
                itemcell?.sprLabel.text = item.spr
                itemcell?.ampLabel.text = item.amp
                itemcell?.voltLabel.text = item.volt
                itemcell?.dprLabel.text = item.dpr
                
                itemcell?.roomtempLabel.text = item.roomtemperature
                itemcell?.ambtemp.text = item.ambtemperature
                
                
                 itemcell?.sduLabel.text = item.sducttemp
                 itemcell?.rduLabel.text = item.rducttemp
                 itemcell?.groupLabel.text = item.group
                 itemcell?.actionLabel.text = item.actiondesc
                 itemcell?.faultLabel.text = item.faultdesc
                
                
                
            }
            return itemcell!;
        }
        
        
        return cell ;
        
    }
    
    
    func getHistoryDetails() {
        SVProgressHUD.show()
        provider.request(.GetWorkOrderDetails(woid: woid!)) { (response) in
            SVProgressHUD.dismiss()
            self.listTableView.spr_endRefreshing()
            switch response{
            case let .success(moyaResponse):
                do {
                    self.woDetails = try moyaResponse.mapObject(WorkOrderDetailsResponse.self)
                    
                    self.listTableView.reloadData()
                }catch let error  {
                    
                }
                
            case let .failure(error):
                print(error)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
