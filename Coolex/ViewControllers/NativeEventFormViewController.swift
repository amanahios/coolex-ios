////
////  ViewController.swift
////  Coolex
////
////  Created by Bassem on 3/11/17.
////  Copyright © 2017 Coolex Dev. All rights reserved.
////
//
//import UIKit
//import Eureka
//import MapKit
//import CoreLocation
//
//enum RepeatInterval : String, CustomStringConvertible {
//    case Never = "Never"
//    case Every_Day = "Every Day"
//    case Every_Week = "Every Week"
//    case Every_2_Weeks = "Every 2 Weeks"
//    case Every_Month = "Every Month"
//    case Every_Year = "Every Year"
//    
//    var description : String { return rawValue }
//    
//    static let allValues = [Never, Every_Day, Every_Week, Every_2_Weeks, Every_Month, Every_Year]
//}
//
//enum EventAlert : String, CustomStringConvertible {
//    case Never = "None"
//    case At_time_of_event = "At time of event"
//    case Five_Minutes = "5 minutes before"
//    case FifTeen_Minutes = "15 minutes before"
//    case Half_Hour = "30 minutes before"
//    case One_Hour = "1 hour before"
//    case Two_Hour = "2 hours before"
//    case One_Day = "1 day before"
//    case Two_Days = "2 days before"
//    
//    var description : String { return rawValue }
//    
//    static let allValues = [Never, At_time_of_event, Five_Minutes, FifTeen_Minutes, Half_Hour, One_Hour, Two_Hour, One_Day, Two_Days]
//}
//
//enum EventState {
//    case busy
//    case free
//    
//    static let allValues = [busy, free]
//}
//
//class NativeEventFormViewController : FormViewController {
//    
//    var woStatus:WOStatus = .started;
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        initializeForm()
//        
//        self.navigationItem.leftBarButtonItem?.target = self
//        self.navigationItem.leftBarButtonItem?.action = #selector(NativeEventFormViewController.cancelTapped(_:))
//    }
//    
//    private func initializeForm() {
//        
//        form +++
//            
//            TextRow("Title").cellSetup { cell, row in
//                cell.textField.placeholder = "Title"
//                var ruleSet = RuleSet<String>()
//                ruleSet.add(rule: RuleRequired())
//                ruleSet.add(rule: RuleEmail())
//                
//                row.add(ruleSet: ruleSet)
//                row.validationOptions = .validatesOnChange
//                
//                }
//                .cellUpdate { cell, row in
//                    if !row.isValid {
//                        cell.titleLabel?.textColor = .red
//                    }else {
//                        cell.titleLabel?.textColor = .black
//                    }
//            }
//            
//            
//            <<< TextRow("refranceID").cellSetup {
//                $1.cell.textField.placeholder = "Refrance ID"
//            }
//            
//            <<< PickerInputRow<String>("category"){
//                
//                $0.add(rule: RuleRequired())
//                
//                
//                $0.title = "category"
//                $0.options = []
//                for i in 1...10{
//                    $0.options.append("category \(i)")
//                }
//                $0.value = $0.options.first
//            }
//            
//            <<< PickerInputRow<String>("priority"){
//                $0.add(rule: RuleRequired())
//                
//                $0.title = "Priority"
//                $0.options = []
//                for i in 1...10{
//                    $0.options.append("Priority \(i)")
//                }
//                $0.value = $0.options.first
//        }
//        
//        
//        if (self.woStatus != WOStatus.notCreated) {
//            
//            
//            form +++ PickerInputRow<String>("status"){
//                $0.add(rule: RuleRequired())
//                
//                $0.title = "Status"
//                $0.options = []
//                for i in 1...4{
//                    $0.options.append("Status \(i)")
//                }
//                $0.value = $0.options.first
//                
//            }
//            
//        }
//        
//        form +++ TextAreaRow("shortDescription") {
//            $0.placeholder = "Short Description"
//            $0.textAreaHeight = .dynamic(initialTextViewHeight: 150)
//            
//            }
//            
//            
//            <<< TextAreaRow("Note") {
//                $0.placeholder = "Notes"
//                $0.textAreaHeight = .dynamic(initialTextViewHeight: 150)
//                
//            }
//            +++
//            
//            EmailRow("externalEmail") {
//                $0.title = "External Email"
//                $0.add(rule: RuleRequired())
//                $0.add(rule: RuleEmail())
//                $0.validationOptions = .validatesOnChangeAfterBlurred
//                }
//                .cellUpdate { cell, row in
//                    if !row.isValid {
//                        cell.titleLabel?.textColor = .red
//                    }
//            }
//            
//            
//            
//            
//            <<< PickerInputRow<String>("department"){
//                $0.title = "Department"
//                $0.options = []
//                for i in 1...10{
//                    $0.options.append("Department \(i)")
//                }
//                $0.value = $0.options.first
//            }
//            
//            
//            <<< PickerInputRow<String>("building"){
//                $0.title = "Building"
//                $0.options = []
//                for i in 1...10{
//                    $0.options.append("Building \(i)")
//                }
//                $0.value = $0.options.first
//            }
//            +++
//            
//            LocationRow("location"){
//                $0.title = "Location"
//                //$0.value = CLLocation(latitude: 30.10060861, longitude: 31.43618402)
//                
//            }
//            +++
//            PickerInputRow<String>("assetes"){
//                $0.title = "Assetes"
//                $0.options = []
//                for i in 1...10{
//                    $0.options.append("Assetes \(i)")
//                }
//                $0.value = $0.options.first
//            }
//            
//            +++
//            SwitchRow() {
//                $0.title = "All Day Task"
//                $0.value = true
//            }
//            
//            <<< DateRow("startDate").cellSetup({ (cell, row) in
//                row.title = "Start Date:"
//                
//                row.add(rule: RuleRequired())
//                
//                
//                
//            })
//        
//        if (self.woStatus != WOStatus.notCreated) {
//            
//            
//            form +++
//                
//                DateRow("dueDate").cellSetup({ (cell, row) in
//                    row.title = "Due Date:"
//                    
//                    row.add(rule: RuleRequired())
//                    
//                    
//                    
//                })
//                
//                
//                <<<   DateRow("completeDate").cellSetup({ (cell, row) in
//                    row.title = "Complete Date:"
//                    
//                    row.add(rule: RuleRequired())
//                    
//                    
//                    
//                })
//                
//                <<<   TimeRow("completeTime").cellSetup({ (cell, row) in
//                    row.title = "Complete Time:"
//                    
//                    row.add(rule: RuleRequired())
//                    
//                    
//                    
//                })
//                
//                <<<   DateRow("closeDate").cellSetup({ (cell, row) in
//                    row.title = "Closed Date:"
//                    
//                    row.add(rule: RuleRequired())
//                    
//                    
//                    
//                })
//                <<<   TimeRow("closeTime").cellSetup({ (cell, row) in
//                    row.title = "Closed Time:"
//                    
//                    row.add(rule: RuleRequired())
//                    
//                    
//                    
//                })
//                
//                <<< LabelRow("totalHours").cellSetup({ (cell, row) in
//                    row.title = "Total Hours"
//                    cell.textLabel?.text = "00:00";
//                })
//                
//                
//                <<< LabelRow("createdBy").cellSetup({ (cell, row) in
//                    row.title = "Created By:"
//                    cell.textLabel?.text = "Mohamed Awadah";
//                })
//            
//            
//        }
//        
//        form +++ ImageRow("attatchImages"){
//            $0.title = "Attatch Images"
//            }
//            
//            
//            +++ ButtonRow() { (row: ButtonRow) -> Void in
//                row.title = "Submit"
//                }
//                .onCellSelection { [weak self] (cell, row) in
//                    self?.printDic()
//        }
//        
//        
//        
//        
//        
//        let valuesDictionary = form.values()
//        
//        print(valuesDictionary)
//        
//    }
//    
//    func cancelTapped(_ barButtonItem: UIBarButtonItem) {
//        //(navigationController as? NativeEventNavigationController)?.onDismissCallback?(self)
//    }
//    
//    @IBAction func printDic(){
//        
//        let error = form.validate()
//        print(error.description)
//        let valuesDictionary = form.values()
//        
//        print(valuesDictionary)
//        
//        
//    }
//    
//}
//
//
//
