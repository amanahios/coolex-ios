//
//  HistoryDetails.swift
//  Coolex
//
//  Created by Bassem Abbas on 8/16/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
class HistoryDetails: UIViewController {
    
    
    
    var woid = ""
     var woDetails:WorkOrderDetailsResponse?
    var cellColor:UIColor =  #colorLiteral(red: 0.3449808884, green: 0.8360603401, blue: 1, alpha: 1)
    var isClosed =  false;
    
     @IBOutlet weak var updateWorkOrderBtn: UIButton!
    
    @IBOutlet weak var woidLabel: UILabel!

    @IBOutlet weak var wonoLabel: UILabel!
    @IBOutlet weak var groupcdLabel: UILabel!
    @IBOutlet weak var complnoLabel: UILabel!
    @IBOutlet weak var statusactionLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    
    @IBOutlet weak var customerNameLabel: UILabel!
//    @IBOutlet weak var customerCdLabel: UILabel!

    @IBOutlet weak var customerMobileLabel: UILabel!
    @IBOutlet weak var customerTelephoneLabel: UILabel!
    
//    @IBOutlet weak var streetNameLabel: UILabel!
//    @IBOutlet weak var blockLabel: UILabel!
//    @IBOutlet weak var addressNameLabel: UILabel!
//    @IBOutlet weak var houseNoLabel: UILabel!

    @IBOutlet weak var latLabel: UILabel!
//    @IBOutlet weak var AreaLabel: UILabel!
    @IBOutlet weak var avenueLabel: UILabel!
    @IBOutlet weak var lngLabel: UILabel!
    


    
    @IBOutlet weak var addEquipmentBtn: UIButton!
    
    @IBOutlet weak var addusedMaterialBtn: UIButton!
    
    override func viewDidLoad() {
        if isClosed == true {
            
            self.addEquipmentBtn.removeFromSuperview();
//            self.updateWorkOrderBtn.removeFromSuperview();
            self.addusedMaterialBtn.removeFromSuperview();
            self.view.layoutIfNeeded();
        }
        if (woDetails != nil) {
            
            self.displayData()
            
//            if ("close" == woDetails?.response?.wostatus?.wostatusname?.lowercased()  ) {
//                self.updateWorkOrderBtn.isEnabled = false;
//
//            }
        }else {
        if !woid.isEmpty {
        self.getHistoryDetails()
        }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = self.cellColor
    }
    
    func getHistoryDetails() {
        SVProgressHUD.show()
        provider.request(.GetWorkOrderDetails(woid: woid)) { (response) in
            SVProgressHUD.dismiss()
            switch response{
            case let .success(moyaResponse):
                do {
                    self.woDetails = try moyaResponse.mapObject(WorkOrderDetailsResponse.self)
                    
                    self.displayData()
                    
                }catch let error  {
                    
                }

            case let .failure(error):
                print(error)
            }
        }
    }
    
    
    @IBAction func listEquipmentAction(_ sender: Any) {
        
        let stb = UIStoryboard(name: "List", bundle: Bundle.main);
        let updateWOStatusViewController = stb.instantiateViewController(withIdentifier: "ListDetailsTableViewController") as! ListDetailsTableViewController
        updateWOStatusViewController.woDetails = self.woDetails
         updateWOStatusViewController.type = "equipment"
        self.navigationController?.pushViewController(updateWOStatusViewController, animated: true)
        
    }
    @IBAction func listMaterialAction(_ sender: Any) {
        
        
        let stb = UIStoryboard(name: "List", bundle: Bundle.main);
        let updateWOStatusViewController = stb.instantiateViewController(withIdentifier: "ListDetailsTableViewController") as! ListDetailsTableViewController
        updateWOStatusViewController.woDetails = self.woDetails
         updateWOStatusViewController.type = "item"
        self.navigationController?.pushViewController(updateWOStatusViewController, animated: true)
        
    }
    func displayData(){
    
        woidLabel.text = self.woDetails?.response?.woid;
        wonoLabel.text = self.woDetails?.response?.wono;
        complnoLabel.text = self.woDetails?.response?.complno;
        groupcdLabel.text = self.woDetails?.response?.groupcd;

        
       remarkLabel.text = self.woDetails?.response?.remarks
        if  let statusactionname = self.woDetails?.response?.wostatusaction?.statusactionname {
            statusactionLabel.text  = statusactionname
            if (statusactionname.lowercased() == "open"){
                statusactionLabel.textColor = .green;
            }else if (statusactionname.lowercased() == "close"){
                statusactionLabel.textColor = .red;
            }else {
                statusactionLabel.textColor = .black;
            }
        }
            
        
       
//
//        

        
     
        
        customerNameLabel.text =    self.woDetails?.response?.customername

//        customerCdLabel.text =    self.woDetails?.response?.custcd
        
        customerMobileLabel.text = self.woDetails?.response?.mobile


        customerTelephoneLabel.text = self.woDetails?.response?.telephone


        avenueLabel.text = self.woDetails?.response?.civilid;

//        addressNameLabel.text = self.woDetails?.response?.addresscivilid
//        streetNameLabel.text = self.woDetails?.response?.street
//        blockLabel.text = self.woDetails?.response?.block
//        houseNoLabel.text = self.woDetails?.response?.houseno
//          AreaLabel.text = self.woDetails?.response?.area;
//        //        latLabel.text = self.woDetails?.response?.customer?.customeraddresses?.first?.geolati
//        //        lngLabel.text = self.woDetails?.response?.customer?.customeraddresses?.first?.geolong
        
      
        
    }
   
    
    @IBAction func updateWorkOrderAction(_ sender: Any) {
        
        
        let stb = UIStoryboard(name: "Main", bundle: Bundle.main);
        let updateWOStatusViewController = stb.instantiateViewController(withIdentifier: "UpdateWOStatusViewController") as! UpdateWOStatusViewController
        updateWOStatusViewController.woDetails = self.woDetails
       updateWOStatusViewController.isClosed = self.isClosed
        self.navigationController?.pushViewController(updateWOStatusViewController, animated: true)
    }
    
    @IBAction func addEquipment(_ sender: Any) {
         let stb = UIStoryboard(name: "AddEquipment", bundle: Bundle.main);
        let addEquipmentParentView = stb.instantiateViewController(withIdentifier: "AddEquipmentViewController") as! AddEquipmentViewController
        addEquipmentParentView.woDetails = self.woDetails
        self.navigationController?.pushViewController(addEquipmentParentView, animated: true)
    }
    
    @IBAction func addUsedItem(_ sender: Any) {
        let stb = UIStoryboard(name: "UsedItem", bundle: Bundle.main);
        
        let addUsedItemViewController = stb.instantiateViewController(withIdentifier: "AddUsedItemViewController") as! AddUsedItemViewController
        addUsedItemViewController.wono = self.woDetails?.response?.wono
        addUsedItemViewController.group = self.woDetails?.response?.groupcd
        addUsedItemViewController.woDetails = self.woDetails;
        self.navigationController?.pushViewController(addUsedItemViewController, animated: true)
    }
    
}
