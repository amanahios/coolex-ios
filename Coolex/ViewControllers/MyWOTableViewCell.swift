//
//  MyWOTableViewCell.swift
//  Coolex
//
//  Created by Amr AbdElAal on 6/20/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import UIKit

class MyWOTableViewCell: UITableViewCell {

    @IBOutlet weak var workOrderName: UILabel!
    @IBOutlet weak var workOrderId: UILabel!
    @IBOutlet weak var dueData: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
