//
//  EqupimentSearchResult.swift
//  Coolex
//
//  Created by germainItLab on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class Fault : NSObject, NSCoding, Mappable{
    
    var faultid : String?
    var faultcode : String?
    var faultdesc : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return Fault()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        faultid <- map["faultid"]
        faultcode <- map["faultcode"]
        faultdesc <- map["faultdesc"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        faultid = aDecoder.decodeObject(forKey: "faultid") as? String
        faultcode = aDecoder.decodeObject(forKey: "faultcode") as? String
        faultdesc = aDecoder.decodeObject(forKey: "faultdesc") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if faultid != nil{
            aCoder.encode(faultid, forKey: "faultid")
        }
        if faultcode != nil{
            aCoder.encode(faultcode, forKey: "faultcode")
        }
        if faultdesc != nil{
            aCoder.encode(faultdesc, forKey: "faultdesc")
        }
    }
    
}
