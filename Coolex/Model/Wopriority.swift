//
//	Wopriority.swift
//
//	Create by Bassem Abbas on 16/8/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Wopriority : NSObject, NSCoding, Mappable{

	var prioritylovid : String?
	var priorityname : String?


	class func newInstance(map: Map) -> Mappable?{
		return Wopriority()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		prioritylovid <- map["prioritylovid"]
		priorityname <- map["priorityname"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         prioritylovid = aDecoder.decodeObject(forKey: "prioritylovid") as? String
         priorityname = aDecoder.decodeObject(forKey: "priorityname") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if prioritylovid != nil{
			aCoder.encode(prioritylovid, forKey: "prioritylovid")
		}
		if priorityname != nil{
			aCoder.encode(priorityname, forKey: "priorityname")
		}

	}

}