//
//  EqupimentSearchResult.swift
//  Coolex
//
//  Created by germainItLab on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class ActionFault : NSObject, NSCoding, Mappable{
    
   
    var actioncode : String?
    var actiondesc : String?
    var actionid : String?
    var faultcode : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return ActionFault()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        actioncode <- map["actioncode"]
        actiondesc <- map["actiondesc"]
        actionid <- map["actionid"]
        faultcode <- map["faultcode"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        actioncode = aDecoder.decodeObject(forKey: "actioncode") as? String
        actiondesc = aDecoder.decodeObject(forKey: "actiondesc") as? String
        actionid = aDecoder.decodeObject(forKey: "actionid") as? String
        faultcode = aDecoder.decodeObject(forKey: "faultcode") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if actioncode != nil{
            aCoder.encode(actioncode, forKey: "actioncode")
        }
        if actiondesc != nil{
            aCoder.encode(actiondesc, forKey: "actiondesc")
        }
        if actionid != nil{
            aCoder.encode(actionid, forKey: "actionid")
        }
        if faultcode != nil{
            aCoder.encode(faultcode, forKey: "faultcode")
        }
        
    }

    
}
