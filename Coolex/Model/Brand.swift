//
//  GetBrand.swift
//  Coolex
//
//  Created by Tayseer on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class Brand : NSObject, NSCoding, Mappable{
    
    var brandcd : String?
    var brandname : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return Brand()
    }
    required init?(map: Map){}
    public override init(){}
    
    func mapping(map: Map)
    {
        brandcd <- map["brandcd"]
        brandname <- map["brandname"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        brandcd = aDecoder.decodeObject(forKey: "brandcd") as? String
        brandname = aDecoder.decodeObject(forKey: "brandname") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if brandcd != nil{
            aCoder.encode(brandcd, forKey: "brandcd")
        }
        if brandname != nil{
            aCoder.encode(brandname, forKey: "priorityname")
        }
        
    }
    
}
