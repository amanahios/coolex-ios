//
//  EqupimentSearchResult.swift
//  Coolex
//
//  Created by germainItLab on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class LovModul : NSObject, NSCoding, Mappable{
    
    var lovid : String?
    var name : String?
    var namear : String?
    var type : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return LovModul()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        lovid <- map["lovid"]
        name <- map["name"]
        namear <- map["namear"]
        type <- map["type"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        lovid = aDecoder.decodeObject(forKey: "lovid") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        namear = aDecoder.decodeObject(forKey: "namear") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if lovid != nil{
            aCoder.encode(lovid, forKey: "lovid")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if namear != nil{
            aCoder.encode(namear, forKey: "namear")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
    }
    
}
