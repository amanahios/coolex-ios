//
//	Equipment.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Equipment : NSObject, NSCoding, Mappable{

	var action : String?
	var ambtemperature : String?
	var amp : String?
	var brandcd : String?
	var dpr : String?
	var fault : String?
	var group : String?
	var modulecd : String?
	var rducttemp : String?
	var roomtemperature : String?
	var sducttemp : String?
	var serialnumber : String?
	var spr : String?
	var volt : String?
	var wono : String?
    var faultdesc : String?
    var actiondesc : String?

	class func newInstance(map: Map) -> Mappable?{
		return Equipment()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		action <- map["action"]
		ambtemperature <- map["ambtemperature"]
		amp <- map["amp"]
		brandcd <- map["brandcd"]
		dpr <- map["dpr"]
		fault <- map["fault"]
		group <- map["group"]
		modulecd <- map["modulecd"]
		rducttemp <- map["rducttemp"]
		roomtemperature <- map["roomtemperature"]
		sducttemp <- map["sducttemp"]
		serialnumber <- map["serialnumber"]
		spr <- map["spr"]
		volt <- map["volt"]
		wono <- map["wono"]
        faultdesc <- map["fault.faultdesc"]
		actiondesc <- map["action.actiondesc"]
        
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         action = aDecoder.decodeObject(forKey: "action") as? String
         ambtemperature = aDecoder.decodeObject(forKey: "ambtemperature") as? String
         amp = aDecoder.decodeObject(forKey: "amp") as? String
         brandcd = aDecoder.decodeObject(forKey: "brandcd") as? String
         dpr = aDecoder.decodeObject(forKey: "dpr") as? String
         fault = aDecoder.decodeObject(forKey: "fault") as? String
         group = aDecoder.decodeObject(forKey: "group") as? String
         modulecd = aDecoder.decodeObject(forKey: "modulecd") as? String
         rducttemp = aDecoder.decodeObject(forKey: "rducttemp") as? String
         roomtemperature = aDecoder.decodeObject(forKey: "roomtemperature") as? String
         sducttemp = aDecoder.decodeObject(forKey: "sducttemp") as? String
         serialnumber = aDecoder.decodeObject(forKey: "serialnumber") as? String
         spr = aDecoder.decodeObject(forKey: "spr") as? String
         volt = aDecoder.decodeObject(forKey: "volt") as? String
         wono = aDecoder.decodeObject(forKey: "wono") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if action != nil{
			aCoder.encode(action, forKey: "action")
		}
		if ambtemperature != nil{
			aCoder.encode(ambtemperature, forKey: "ambtemperature")
		}
		if amp != nil{
			aCoder.encode(amp, forKey: "amp")
		}
		if brandcd != nil{
			aCoder.encode(brandcd, forKey: "brandcd")
		}
		if dpr != nil{
			aCoder.encode(dpr, forKey: "dpr")
		}
		if fault != nil{
			aCoder.encode(fault, forKey: "fault")
		}
		if group != nil{
			aCoder.encode(group, forKey: "group")
		}
		if modulecd != nil{
			aCoder.encode(modulecd, forKey: "modulecd")
		}
		if rducttemp != nil{
			aCoder.encode(rducttemp, forKey: "rducttemp")
		}
		if roomtemperature != nil{
			aCoder.encode(roomtemperature, forKey: "roomtemperature")
		}
		if sducttemp != nil{
			aCoder.encode(sducttemp, forKey: "sducttemp")
		}
		if serialnumber != nil{
			aCoder.encode(serialnumber, forKey: "serialnumber")
		}
		if spr != nil{
			aCoder.encode(spr, forKey: "spr")
		}
		if volt != nil{
			aCoder.encode(volt, forKey: "volt")
		}
		if wono != nil{
			aCoder.encode(wono, forKey: "wono")
		}

	}

}
