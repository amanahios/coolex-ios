//
//	HistoryDetails.swift
//
//	Create by Bassem Abbas on 16/8/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class HistoryDetailsModel : NSObject, NSCoding, Mappable{

	var complno : String?
	var custcd : String?
	var customer : Customer?
	var groupcd : String?
	var manualwono : AnyObject?
	var techtimein : String?
	var techtimeout : String?
	var woid : String?
	var wono : String?
	var wostatus : Wostatu?
	var wostatusaction : Wostatusaction?


	class func newInstance(map: Map) -> Mappable?{
		return HistoryDetailsModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		complno <- map["complno"]
		custcd <- map["custcd"]
		customer <- map["customer"]
		groupcd <- map["groupcd"]
		manualwono <- map["manualwono"]
		techtimein <- map["techtimein"]
		techtimeout <- map["techtimeout"]
		woid <- map["woid"]
		wono <- map["wono"]
		wostatus <- map["wostatus"]
		wostatusaction <- map["wostatusaction"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         complno = aDecoder.decodeObject(forKey: "complno") as? String
         custcd = aDecoder.decodeObject(forKey: "custcd") as? String
         customer = aDecoder.decodeObject(forKey: "customer") as? Customer
         groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
         manualwono = aDecoder.decodeObject(forKey: "manualwono") as? AnyObject
         techtimein = aDecoder.decodeObject(forKey: "techtimein") as? String
         techtimeout = aDecoder.decodeObject(forKey: "techtimeout") as? String
         woid = aDecoder.decodeObject(forKey: "woid") as? String
         wono = aDecoder.decodeObject(forKey: "wono") as? String
         wostatus = aDecoder.decodeObject(forKey: "wostatus") as? Wostatu
         wostatusaction = aDecoder.decodeObject(forKey: "wostatusaction") as? Wostatusaction

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if complno != nil{
			aCoder.encode(complno, forKey: "complno")
		}
		if custcd != nil{
			aCoder.encode(custcd, forKey: "custcd")
		}
		if customer != nil{
			aCoder.encode(customer, forKey: "customer")
		}
		if groupcd != nil{
			aCoder.encode(groupcd, forKey: "groupcd")
		}
		if manualwono != nil{
			aCoder.encode(manualwono, forKey: "manualwono")
		}
		if techtimein != nil{
			aCoder.encode(techtimein, forKey: "techtimein")
		}
		if techtimeout != nil{
			aCoder.encode(techtimeout, forKey: "techtimeout")
		}
		if woid != nil{
			aCoder.encode(woid, forKey: "woid")
		}
		if wono != nil{
			aCoder.encode(wono, forKey: "wono")
		}
		if wostatus != nil{
			aCoder.encode(wostatus, forKey: "wostatus")
		}
		if wostatusaction != nil{
			aCoder.encode(wostatusaction, forKey: "wostatusaction")
		}

	}

}
