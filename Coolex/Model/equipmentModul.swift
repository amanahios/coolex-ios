//
//  GetBrand.swift
//  Coolex
//
//  Created by Tayseer on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class equipmentModul : NSObject, NSCoding, Mappable{
    
    var brandcd : String?
    var modulecd : String?
    var modulename : String?
    var capacity : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return equipmentModul()
    }
    required init?(map: Map){}
    public override init(){}
    
    func mapping(map: Map)
    {
        brandcd <- map["brandcd"]
        modulecd <- map["modulecd"]
        modulename <- map["modulename"]
        capacity <- map["capacity"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        brandcd = aDecoder.decodeObject(forKey: "brandcd") as? String
        modulecd = aDecoder.decodeObject(forKey: "modulecd") as? String
        modulename = aDecoder.decodeObject(forKey: "modulename") as? String
        capacity = aDecoder.decodeObject(forKey: "capacity") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if brandcd != nil{
            aCoder.encode(brandcd, forKey: "brandcd")
        }
        if modulecd != nil{
            aCoder.encode(modulecd, forKey: "modulecd")
        }
        if modulename != nil{
            aCoder.encode(modulename, forKey: "modulename")
        }
        if capacity != nil{
            aCoder.encode(capacity, forKey: "capacity")
        }
        
    }
    
}
