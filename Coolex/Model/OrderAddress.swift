//
//	OrderAddress.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class OrderAddress : NSObject, NSCoding, Mappable{

	var area : String?
	var avenue : String?
	var block : String?
	var houseno : String?
	var plotNo : String?
	var street : String?


	class func newInstance(map: Map) -> Mappable?{
		return OrderAddress()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		area <- map["area"]
		avenue <- map["avenue"]
		block <- map["block"]
		houseno <- map["houseno"]
		plotNo <- map["plot_no"]
		street <- map["street"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         area = aDecoder.decodeObject(forKey: "area") as? String
         avenue = aDecoder.decodeObject(forKey: "avenue") as? String
         block = aDecoder.decodeObject(forKey: "block") as? String
         houseno = aDecoder.decodeObject(forKey: "houseno") as? String
         plotNo = aDecoder.decodeObject(forKey: "plot_no") as? String
         street = aDecoder.decodeObject(forKey: "street") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if area != nil{
			aCoder.encode(area, forKey: "area")
		}
		if avenue != nil{
			aCoder.encode(avenue, forKey: "avenue")
		}
		if block != nil{
			aCoder.encode(block, forKey: "block")
		}
		if houseno != nil{
			aCoder.encode(houseno, forKey: "houseno")
		}
		if plotNo != nil{
			aCoder.encode(plotNo, forKey: "plot_no")
		}
		if street != nil{
			aCoder.encode(street, forKey: "street")
		}

	}

}