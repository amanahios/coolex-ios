//
//	Item.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Item : NSObject, NSCoding, Mappable{

	var itemcode : String?
	var group : String?
	var module : String?
	var price : String?
	var quantity : String?
	var uom : String?
	var wono : String?


	class func newInstance(map: Map) -> Mappable?{
		return Item()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		itemcode <- map["Itemcode"]
		group <- map["group"]
		module <- map["module"]
		price <- map["price"]
		quantity <- map["quantity"]
		uom <- map["uom"]
		wono <- map["wono"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         itemcode = aDecoder.decodeObject(forKey: "Itemcode") as? String
         group = aDecoder.decodeObject(forKey: "group") as? String
         module = aDecoder.decodeObject(forKey: "module") as? String
         price = aDecoder.decodeObject(forKey: "price") as? String
         quantity = aDecoder.decodeObject(forKey: "quantity") as? String
         uom = aDecoder.decodeObject(forKey: "uom") as? String
         wono = aDecoder.decodeObject(forKey: "wono") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if itemcode != nil{
			aCoder.encode(itemcode, forKey: "Itemcode")
		}
		if group != nil{
			aCoder.encode(group, forKey: "group")
		}
		if module != nil{
			aCoder.encode(module, forKey: "module")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if uom != nil{
			aCoder.encode(uom, forKey: "uom")
		}
		if wono != nil{
			aCoder.encode(wono, forKey: "wono")
		}

	}

}
