//
//  EqupimentSearchResult.swift
//  Coolex
//
//  Created by germainItLab on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class EqupimentSearchResult : NSObject, NSCoding, Mappable{
    
    var brand : String?
    var itemcode : String?
    var module : String?
    var serialnumber : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return EqupimentSearchResult()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        brand <- map["brand"]
        itemcode <- map["itemcode"]
        module <- map["module"]
        serialnumber <- map["serialnumber"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        brand = aDecoder.decodeObject(forKey: "brand") as? String
        itemcode = aDecoder.decodeObject(forKey: "itemcode") as? String
        module = aDecoder.decodeObject(forKey: "module") as? String
        serialnumber = aDecoder.decodeObject(forKey: "serialnumber") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if brand != nil{
            aCoder.encode(brand, forKey: "brand")
        }
        if itemcode != nil{
            aCoder.encode(itemcode, forKey: "itemcode")
        }
        if module != nil{
            aCoder.encode(module, forKey: "module")
        }
        if serialnumber != nil{
            aCoder.encode(serialnumber, forKey: "serialnumber")
        }
        
    }

    
}
