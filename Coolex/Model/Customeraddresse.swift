//
//	Customeraddresse.swift
//
//	Create by Bassem Abbas on 16/8/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Customeraddresse : NSObject, NSCoding, Mappable{

	var addressname : String?
	var area : String?
	var avenue : String?
	var block : String?
	var custcd : String?
	var customeraddressid : String?
	var geolati : String?
	var geolong : String?
	var houseno : String?
	var plotNo : AnyObject?
	var street : String?


	class func newInstance(map: Map) -> Mappable?{
		return Customeraddresse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		addressname <- map["addressname"]
		area <- map["area"]
		avenue <- map["avenue"]
		block <- map["block"]
		custcd <- map["custcd"]
		customeraddressid <- map["customeraddressid"]
		geolati <- map["geolati"]
		geolong <- map["geolong"]
		houseno <- map["houseno"]
		plotNo <- map["plot_no"]
		street <- map["street"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressname = aDecoder.decodeObject(forKey: "addressname") as? String
         area = aDecoder.decodeObject(forKey: "area") as? String
         avenue = aDecoder.decodeObject(forKey: "avenue") as? String
         block = aDecoder.decodeObject(forKey: "block") as? String
         custcd = aDecoder.decodeObject(forKey: "custcd") as? String
         customeraddressid = aDecoder.decodeObject(forKey: "customeraddressid") as? String
         geolati = aDecoder.decodeObject(forKey: "geolati") as? String
         geolong = aDecoder.decodeObject(forKey: "geolong") as? String
         houseno = aDecoder.decodeObject(forKey: "houseno") as? String
         plotNo = aDecoder.decodeObject(forKey: "plot_no") as? AnyObject
         street = aDecoder.decodeObject(forKey: "street") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if addressname != nil{
			aCoder.encode(addressname, forKey: "addressname")
		}
		if area != nil{
			aCoder.encode(area, forKey: "area")
		}
		if avenue != nil{
			aCoder.encode(avenue, forKey: "avenue")
		}
		if block != nil{
			aCoder.encode(block, forKey: "block")
		}
		if custcd != nil{
			aCoder.encode(custcd, forKey: "custcd")
		}
		if customeraddressid != nil{
			aCoder.encode(customeraddressid, forKey: "customeraddressid")
		}
		if geolati != nil{
			aCoder.encode(geolati, forKey: "geolati")
		}
		if geolong != nil{
			aCoder.encode(geolong, forKey: "geolong")
		}
		if houseno != nil{
			aCoder.encode(houseno, forKey: "houseno")
		}
		if plotNo != nil{
			aCoder.encode(plotNo, forKey: "plot_no")
		}
		if street != nil{
			aCoder.encode(street, forKey: "street")
		}

	}

}