

import Foundation
import ObjectMapper


class Group : NSObject, NSCoding, Mappable{
    
    var employeename : String?
    var employeeno : String?
    var groupcd : String?
    var groupdesc : String?
    var groupid : String?
    var remarks : String?
    var vandesc : String?
    var vanno : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return Group()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        employeename <- map["employeename"]
        employeeno <- map["employeeno"]
        groupcd <- map["groupcd"]
        groupdesc <- map["groupdesc"]
        groupid <- map["groupid"]
        remarks <- map["remarks"]
        vandesc <- map["vandesc"]
        vanno <- map["vanno"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        employeename = aDecoder.decodeObject(forKey: "employeename") as? String
        employeeno = aDecoder.decodeObject(forKey: "employeeno") as? String
        groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
        groupdesc = aDecoder.decodeObject(forKey: "groupdesc") as? String
        groupid = aDecoder.decodeObject(forKey: "groupid") as? String
        remarks = aDecoder.decodeObject(forKey: "remarks") as? String
        vandesc = aDecoder.decodeObject(forKey: "vandesc") as? String
        vanno = aDecoder.decodeObject(forKey: "vanno") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if employeename != nil{
            aCoder.encode(employeename, forKey: "employeename")
        }
        if employeeno != nil{
            aCoder.encode(employeeno, forKey: "employeeno")
        }
        if groupcd != nil{
            aCoder.encode(groupcd, forKey: "groupcd")
        }
        if groupdesc != nil{
            aCoder.encode(groupdesc, forKey: "groupdesc")
        }
        if groupid != nil{
            aCoder.encode(groupid, forKey: "groupid")
        }
        if remarks != nil{
            aCoder.encode(remarks, forKey: "remarks")
        }
        if vandesc != nil{
            aCoder.encode(vandesc, forKey: "vandesc")
        }
        if vanno != nil{
            aCoder.encode(vanno, forKey: "vanno")
        }
        
    }
    
}
