//
//  EqupimentSearchResult.swift
//  Coolex
//
//  Created by germainItLab on 8/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import ObjectMapper


class ItemDescription : NSObject, NSCoding, Mappable{
    var itemcode : String?
    var itemname : String?
    var uom : String?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return ItemDescription()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        itemcode <- map["itemcode"]
        itemname <- map["itemname"]
        uom <- map["uom"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        itemcode = aDecoder.decodeObject(forKey: "itemcode") as? String
        itemname = aDecoder.decodeObject(forKey: "itemname") as? String
        uom = aDecoder.decodeObject(forKey: "uom") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        
        if itemcode != nil{
            aCoder.encode(itemname, forKey: "itemcode")
        }
        if itemname != nil{
            aCoder.encode(itemname, forKey: "itemname")
        }
        if uom != nil{
            aCoder.encode(uom, forKey: "uom")
        }
        
    }
    
}
