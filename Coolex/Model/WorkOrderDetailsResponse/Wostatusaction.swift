//
//	Wostatusaction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Wostatusaction : NSObject, NSCoding, Mappable{

	var statusactionname : String?
	var wostatusactionid : String?
	//var wostatuslovid : String?


	class func newInstance(map: Map) -> Mappable?{
		return Wostatusaction()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		statusactionname <- map["statusactionname"]
		wostatusactionid <- map["wostatusactionid"]
		//wostatuslovid <- map["wostatuslovid"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         statusactionname = aDecoder.decodeObject(forKey: "statusactionname") as? String
         wostatusactionid = aDecoder.decodeObject(forKey: "wostatusactionid") as? String
         //wostatuslovid = aDecoder.decodeObject(forKey: "wostatuslovid") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if statusactionname != nil{
			aCoder.encode(statusactionname, forKey: "statusactionname")
		}
		if wostatusactionid != nil{
			aCoder.encode(wostatusactionid, forKey: "wostatusactionid")
		}
//        if wostatuslovid != nil{
//            aCoder.encode(wostatuslovid, forKey: "wostatuslovid")
//        }

	}

}
