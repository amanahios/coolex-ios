//
//	Statu.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Statu : NSObject, NSCoding, Mappable{

	var info1 : String?
	var info2 : String?
	var statuscode : String?
	var statusdescription : String?


	class func newInstance(map: Map) -> Mappable?{
		return Statu()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		info1 <- map["info1"]
		info2 <- map["info2"]
		statuscode <- map["statuscode"]
		statusdescription <- map["statusdescription"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         info1 = aDecoder.decodeObject(forKey: "info1") as? String
         info2 = aDecoder.decodeObject(forKey: "info2") as? String
         statuscode = aDecoder.decodeObject(forKey: "statuscode") as? String
         statusdescription = aDecoder.decodeObject(forKey: "statusdescription") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if info1 != nil{
			aCoder.encode(info1, forKey: "info1")
		}
		if info2 != nil{
			aCoder.encode(info2, forKey: "info2")
		}
		if statuscode != nil{
			aCoder.encode(statuscode, forKey: "statuscode")
		}
		if statusdescription != nil{
			aCoder.encode(statusdescription, forKey: "statusdescription")
		}

	}

}