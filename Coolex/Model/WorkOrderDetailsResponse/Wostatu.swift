//
//	Wostatu.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Wostatu : NSObject, NSCoding, Mappable{

	var wostatuslovid : String?
	var wostatusname : String?


	class func newInstance(map: Map) -> Mappable?{
		return Wostatu()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		wostatuslovid <- map["wostatuslovid"]
		wostatusname <- map["wostatusname"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         wostatuslovid = aDecoder.decodeObject(forKey: "wostatuslovid") as? String
         wostatusname = aDecoder.decodeObject(forKey: "wostatusname") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if wostatuslovid != nil{
			aCoder.encode(wostatuslovid, forKey: "wostatuslovid")
		}
		if wostatusname != nil{
			aCoder.encode(wostatusname, forKey: "wostatusname")
		}

	}

}