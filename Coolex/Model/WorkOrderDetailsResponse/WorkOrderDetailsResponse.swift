//
//	WorkOrderDetailsResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class WorkOrderDetailsResponse : NSObject, NSCoding, Mappable{

	var objname : String?
	var response : Response?
	var status : Statu?


	class func newInstance(map: Map) -> Mappable?{
		return WorkOrderDetailsResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		objname <- map["objname"]
		response <- map["response"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         objname = aDecoder.decodeObject(forKey: "objname") as? String
         response = aDecoder.decodeObject(forKey: "response") as? Response
         status = aDecoder.decodeObject(forKey: "status") as? Statu

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if objname != nil{
			aCoder.encode(objname, forKey: "objname")
		}
		if response != nil{
			aCoder.encode(response, forKey: "response")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
