//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Response : NSObject, NSCoding, Mappable{

    var addresscivilid : String?
    var area : String?
    var avenue : String?
    var block : String?
    var civilid : String?
    var complno : String?
    var contractno : String?
    var custcd : String?
    var customername : String?
    var groupcd : String?
    var houseno : String?
    var manualwono : String?
    var mobile : String?
    var plotNo : String?
    var remarks : String?
    var servicetype : String?
    var street : String?
    var techtimein : AnyObject?
    var techtimeout : AnyObject?
    var telephone : String?
    var type : String?
    var woid : String?
    var wono : String?
	var wostatus : Wostatu?
	var wostatusaction : Wostatusaction?
    var equipment : [Equipment]?
    var items : [Item]?

	class func newInstance(map: Map) -> Mappable?{
		return Response()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
        addresscivilid <- map["addresscivilid"]
        area <- map["area"]
        avenue <- map["avenue"]
        block <- map["block"]
        civilid <- map["civilid"]
        complno <- map["complno"]
        contractno <- map["contractno"]
        custcd <- map["custcd"]
        customername <- map["customername"]
        equipment <- map["equipment"]
        groupcd <- map["groupcd"]
        houseno <- map["houseno"]
        items <- map["items"]
        manualwono <- map["manualwono"]
        mobile <- map["mobile"]
        plotNo <- map["plot_no"]
        remarks <- map["remarks"]
        servicetype <- map["servicetype"]
        street <- map["street"]
        techtimein <- map["techtimein"]
        techtimeout <- map["techtimeout"]
        telephone <- map["telephone"]
        type <- map["type"]
        woid <- map["woid"]
        wono <- map["wono"]
        wostatusaction <- map["wostatusaction"]

		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        addresscivilid = aDecoder.decodeObject(forKey: "addresscivilid") as? String
        area = aDecoder.decodeObject(forKey: "area") as? String
        avenue = aDecoder.decodeObject(forKey: "avenue") as? String
        block = aDecoder.decodeObject(forKey: "block") as? String
        civilid = aDecoder.decodeObject(forKey: "civilid") as? String
        complno = aDecoder.decodeObject(forKey: "complno") as? String
        contractno = aDecoder.decodeObject(forKey: "contractno") as? String
        custcd = aDecoder.decodeObject(forKey: "custcd") as? String
        customername = aDecoder.decodeObject(forKey: "customername") as? String
        equipment = aDecoder.decodeObject(forKey: "equipment") as? [Equipment]
        groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
        houseno = aDecoder.decodeObject(forKey: "houseno") as? String
        items = aDecoder.decodeObject(forKey: "items") as? [Item]
        manualwono = aDecoder.decodeObject(forKey: "manualwono") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        plotNo = aDecoder.decodeObject(forKey: "plot_no") as? String
        remarks = aDecoder.decodeObject(forKey: "remarks") as? String
        servicetype = aDecoder.decodeObject(forKey: "servicetype") as? String
        street = aDecoder.decodeObject(forKey: "street") as? String
        techtimein = aDecoder.decodeObject(forKey: "techtimein") as? AnyObject
        techtimeout = aDecoder.decodeObject(forKey: "techtimeout") as? AnyObject
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        woid = aDecoder.decodeObject(forKey: "woid") as? String
        wono = aDecoder.decodeObject(forKey: "wono") as? String
        wostatusaction = aDecoder.decodeObject(forKey: "wostatusaction") as? Wostatusaction

        


	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
        if addresscivilid != nil{
            aCoder.encode(addresscivilid, forKey: "addresscivilid")
        }
        if area != nil{
            aCoder.encode(area, forKey: "area")
        }
        if avenue != nil{
            aCoder.encode(avenue, forKey: "avenue")
        }
        if block != nil{
            aCoder.encode(block, forKey: "block")
        }
        if civilid != nil{
            aCoder.encode(civilid, forKey: "civilid")
        }
        if complno != nil{
            aCoder.encode(complno, forKey: "complno")
        }
        if contractno != nil{
            aCoder.encode(contractno, forKey: "contractno")
        }
        if custcd != nil{
            aCoder.encode(custcd, forKey: "custcd")
        }
        if customername != nil{
            aCoder.encode(customername, forKey: "customername")
        }
        if equipment != nil{
            aCoder.encode(equipment, forKey: "equipment")
        }
        if groupcd != nil{
            aCoder.encode(groupcd, forKey: "groupcd")
        }
        if houseno != nil{
            aCoder.encode(houseno, forKey: "houseno")
        }
        if items != nil{
            aCoder.encode(items, forKey: "items")
        }
        if manualwono != nil{
            aCoder.encode(manualwono, forKey: "manualwono")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if plotNo != nil{
            aCoder.encode(plotNo, forKey: "plot_no")
        }
        if remarks != nil{
            aCoder.encode(remarks, forKey: "remarks")
        }
        if servicetype != nil{
            aCoder.encode(servicetype, forKey: "servicetype")
        }
        if street != nil{
            aCoder.encode(street, forKey: "street")
        }
        if techtimein != nil{
            aCoder.encode(techtimein, forKey: "techtimein")
        }
        if techtimeout != nil{
            aCoder.encode(techtimeout, forKey: "techtimeout")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if woid != nil{
            aCoder.encode(woid, forKey: "woid")
        }
        if wono != nil{
            aCoder.encode(wono, forKey: "wono")
        }
        if wostatusaction != nil{
            aCoder.encode(wostatusaction, forKey: "wostatusaction")
        }

	}

}
