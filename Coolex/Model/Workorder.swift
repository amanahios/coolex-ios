//
//	Workorder.swift
//
//	Create by Bassem Abbas on 16/8/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Workorder :NSObject, NSCoding, Mappable{
        
    var complno : String?
    var contractInfo : ContractInfo?
    var contractno : String?
    var custcd : String?
    var customerInfo : CustomerInfo?
    var customerremark : String?
    var groupcd : String?
    var manualwono : String?
    var orderAddress : OrderAddress?
    var servicetype : String?
    var techremark : String?
    var type : String?
    var woid : String?
    var wono : String?
    var wostatusaction : Wostatusaction?


    class func newInstance(map: Map) -> Mappable?{
        return Workorder()
    }
    required init?(map: Map){}
    private override init(){}


    func mapping(map: Map)
    {
        complno <- map["complno"]
        contractInfo <- map["contractInfo"]
        contractno <- map["contractno"]
        custcd <- map["custcd"]
        customerInfo <- map["customerInfo"]
        customerremark <- map["customerremark"]
        groupcd <- map["groupcd"]
        manualwono <- map["manualwono"]
        orderAddress <- map["orderAddress"]
        servicetype <- map["servicetype"]
        techremark <- map["techremark"]
        type <- map["type"]
        woid <- map["woid"]
        wono <- map["wono"]
        wostatusaction <- map["wostatusaction"]

    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        complno = aDecoder.decodeObject(forKey: "complno") as? String
        contractInfo = aDecoder.decodeObject(forKey: "contractInfo") as? ContractInfo
        contractno = aDecoder.decodeObject(forKey: "contractno") as? String
        custcd = aDecoder.decodeObject(forKey: "custcd") as? String
        customerInfo = aDecoder.decodeObject(forKey: "customerInfo") as? CustomerInfo
        customerremark = aDecoder.decodeObject(forKey: "customerremark") as? String
        groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
        manualwono = aDecoder.decodeObject(forKey: "manualwono") as? String
        orderAddress = aDecoder.decodeObject(forKey: "orderAddress") as? OrderAddress
        servicetype = aDecoder.decodeObject(forKey: "servicetype") as? String
        techremark = aDecoder.decodeObject(forKey: "techremark") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        woid = aDecoder.decodeObject(forKey: "woid") as? String
        wono = aDecoder.decodeObject(forKey: "wono") as? String
        wostatusaction = aDecoder.decodeObject(forKey: "wostatusaction") as? Wostatusaction

    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if complno != nil{
            aCoder.encode(complno, forKey: "complno")
        }
        if contractInfo != nil{
            aCoder.encode(contractInfo, forKey: "contractInfo")
        }
        if contractno != nil{
            aCoder.encode(contractno, forKey: "contractno")
        }
        if custcd != nil{
            aCoder.encode(custcd, forKey: "custcd")
        }
        if customerInfo != nil{
            aCoder.encode(customerInfo, forKey: "customerInfo")
        }
        if customerremark != nil{
            aCoder.encode(customerremark, forKey: "customerremark")
        }
        if groupcd != nil{
            aCoder.encode(groupcd, forKey: "groupcd")
        }
        if manualwono != nil{
            aCoder.encode(manualwono, forKey: "manualwono")
        }
        if orderAddress != nil{
            aCoder.encode(orderAddress, forKey: "orderAddress")
        }
        if servicetype != nil{
            aCoder.encode(servicetype, forKey: "servicetype")
        }
        if techremark != nil{
            aCoder.encode(techremark, forKey: "techremark")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if woid != nil{
            aCoder.encode(woid, forKey: "woid")
        }
        if wono != nil{
            aCoder.encode(wono, forKey: "wono")
        }
        if wostatusaction != nil{
            aCoder.encode(wostatusaction, forKey: "wostatusaction")
        }

    }


}


class CustomerInfo : NSObject, NSCoding, Mappable{

    var custcd : String?
    var customername : String?
    var mobile : String?
    var telephone : String?


    class func newInstance(map: Map) -> Mappable?{
        return CustomerInfo()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        custcd <- map["custcd"]
        customername <- map["customername"]
        mobile <- map["mobile"]
        telephone <- map["telephone"]

    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        custcd = aDecoder.decodeObject(forKey: "custcd") as? String
        customername = aDecoder.decodeObject(forKey: "customername") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String

    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if custcd != nil{
            aCoder.encode(custcd, forKey: "custcd")
        }
        if customername != nil{
            aCoder.encode(customername, forKey: "customername")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }

    }

}




class ContractInfo : NSObject, NSCoding, Mappable{

    var expirationDate : String?
    var projectNo : String?


    class func newInstance(map: Map) -> Mappable?{
        return ContractInfo()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        expirationDate <- map["ExpirationDate"]
        projectNo <- map["ProjectNo"]

    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        expirationDate = aDecoder.decodeObject(forKey: "ExpirationDate") as? String
        projectNo = aDecoder.decodeObject(forKey: "ProjectNo") as? String

    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if expirationDate != nil{
            aCoder.encode(expirationDate, forKey: "ExpirationDate")
        }
        if projectNo != nil{
            aCoder.encode(projectNo, forKey: "ProjectNo")
        }

    }

}
