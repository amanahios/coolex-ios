//
//	ContractItem.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ContractItem : NSObject, NSCoding, Mappable{

	var itmCd : String?


	class func newInstance(map: Map) -> Mappable?{
		return ContractItem()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		itmCd <- map["itm_cd"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         itmCd = aDecoder.decodeObject(forKey: "itm_cd") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if itmCd != nil{
			aCoder.encode(itmCd, forKey: "itm_cd")
		}

	}

}