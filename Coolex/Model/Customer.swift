//
//	Customer.swift
//
//	Create by Bassem Abbas on 16/8/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Customer : NSObject, NSCoding, Mappable{

	var custcd : String?
	var customeraddresses : [Customeraddresse]?
	var customerid : String?
	var customername : String?
	var mobile : String?
	var telephone : String?


	class func newInstance(map: Map) -> Mappable?{
		return Customer()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		custcd <- map["custcd"]
		customeraddresses <- map["customeraddresses"]
		customerid <- map["customerid"]
		customername <- map["customername"]
		mobile <- map["mobile"]
		telephone <- map["telephone"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         custcd = aDecoder.decodeObject(forKey: "custcd") as? String
         customeraddresses = aDecoder.decodeObject(forKey: "customeraddresses") as? [Customeraddresse]
         customerid = aDecoder.decodeObject(forKey: "customerid") as? String
         customername = aDecoder.decodeObject(forKey: "customername") as? String
         mobile = aDecoder.decodeObject(forKey: "mobile") as? String
         telephone = aDecoder.decodeObject(forKey: "telephone") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if custcd != nil{
			aCoder.encode(custcd, forKey: "custcd")
		}
		if customeraddresses != nil{
			aCoder.encode(customeraddresses, forKey: "customeraddresses")
		}
		if customerid != nil{
			aCoder.encode(customerid, forKey: "customerid")
		}
		if customername != nil{
			aCoder.encode(customername, forKey: "customername")
		}
		if mobile != nil{
			aCoder.encode(mobile, forKey: "mobile")
		}
		if telephone != nil{
			aCoder.encode(telephone, forKey: "telephone")
		}

	}

}