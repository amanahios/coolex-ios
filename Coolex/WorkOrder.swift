//
//	WorkOrder.swift
//
//	Create by Amr Abd El-Aal on 20/6/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class WorkOrder : NSObject, NSCoding{

	var complno : String!
	var custcd : String!
	var groupcd : String!
	var manualwono : AnyObject!
	var woid : String!
	var wono : String!
	var wostatus : AnyObject!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		complno = json["complno"].stringValue
		custcd = json["custcd"].stringValue
		groupcd = json["groupcd"].stringValue
		manualwono = json["manualwono"].stringValue as AnyObject
		woid = json["woid"].stringValue
		wono = json["wono"].stringValue
		wostatus = json["wostatus"].stringValue as AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if complno != nil{
			dictionary["complno"] = complno
		}
		if custcd != nil{
			dictionary["custcd"] = custcd
		}
		if groupcd != nil{
			dictionary["groupcd"] = groupcd
		}
		if manualwono != nil{
			dictionary["manualwono"] = manualwono
		}
		if woid != nil{
			dictionary["woid"] = woid
		}
		if wono != nil{
			dictionary["wono"] = wono
		}
		if wostatus != nil{
			dictionary["wostatus"] = wostatus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         complno = aDecoder.decodeObject(forKey: "complno") as? String
         custcd = aDecoder.decodeObject(forKey: "custcd") as? String
         groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
         manualwono = aDecoder.decodeObject(forKey: "manualwono") as? AnyObject
         woid = aDecoder.decodeObject(forKey: "woid") as? String
         wono = aDecoder.decodeObject(forKey: "wono") as? String
         wostatus = aDecoder.decodeObject(forKey: "wostatus") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if complno != nil{
			aCoder.encode(complno, forKey: "complno")
		}
		if custcd != nil{
			aCoder.encode(custcd, forKey: "custcd")
		}
		if groupcd != nil{
			aCoder.encode(groupcd, forKey: "groupcd")
		}
		if manualwono != nil{
			aCoder.encode(manualwono, forKey: "manualwono")
		}
		if woid != nil{
			aCoder.encode(woid, forKey: "woid")
		}
		if wono != nil{
			aCoder.encode(wono, forKey: "wono")
		}
		if wostatus != nil{
			aCoder.encode(wostatus, forKey: "wostatus")
		}

	}

}
