//
//  WOStatus.swift
//  Coolex
//
//  Created by Bassem on 3/25/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation

enum WOStatus:Int {
    case notCreated = 0
    case created
    case pending
    case started
    case finished
}
