//
//  Manager.swift
//  Coolex
//
//  Created by Amr AbdElAal on 5/9/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import Alamofire
import Moya
import Moya_ObjectMapper


let provider = MoyaProvider<CoolexEndPoints>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter),CredentialsPlugin { _ -> URLCredential? in
    return URLCredential(user: "coolexappuser", password: "coolex@app969", persistence: .none)
    }
    ])
//,AuthPlugin(token:"Y29vbGV4YXBwdXNlcjpjb29sZXhAcHA5Njk=")

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}


struct AuthPlugin: PluginType {
    let token: String

    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var request = request
        request.addValue("Basic " + token, forHTTPHeaderField: "Authorization")
        return request
    }
}

public enum CoolexEndPoints {
    case Login(email:String,password:String)
    case GetWorkOrdersByGroup(groupcd:String)
    case GetClosedWorkOrdersByGroup(groupcd:String)
    case GetClosedWorkOrdersbySearch(groupcd:String,wono:String,complno:String,custcd:String)
    case GetWorkOrderStatusAction(wostatuslovid:String)
    case GetWorkOrderDetails(woid:String)
    case GetAllBrand()
    case GetModules(brandcd:String)
    case GetByBrandandModule(brand:String,module:String)
    case GetBySerialNumber(serialNumber: String)
    case AddEquipment(dic: [String:String])
    case GetGroup()
    case GetFaults()
    case GetActionFaults(faultcode:String)
    case addUsedItems ([String:String])
    case getStatus(type: String)
    case UpdateWorkOrderDetails ([String:String])
    case GetItemDescription(query:String)

    case SearchBySerialNumber(query:String)
    case SearchModuleByName(query:String)
    case SearchBrandByName(query:String)

    case getContractUnits(contractcd:String)
    case getContractDevices(contractcd:String,itm_cd:String)
}


extension CoolexEndPoints : TargetType {
    public var task: Task {

        switch self {
        case .Login(email: let email, password: let pas):
            return .requestParameters(parameters: ["input": ["username":email,"password":pas]] , encoding: JSONEncoding.default)
        case .GetWorkOrdersByGroup(groupcd:let groupcd):
            return .requestParameters(parameters: ["input":["groupcd":groupcd]], encoding: JSONEncoding.default)
        case .GetClosedWorkOrdersByGroup(groupcd:let groupcd):
            return .requestParameters(parameters: ["input":["groupcd":groupcd]], encoding: JSONEncoding.default)
        case .GetClosedWorkOrdersbySearch(groupcd:let groupcd ,wono:let  wono,complno:let  complno,custcd:let  custcd) :
            return .requestParameters(parameters: ["input":["groupcd":groupcd , "wono":wono , "complno" : complno , "custcd" : custcd]], encoding: JSONEncoding.default)
        case .GetWorkOrderStatusAction(wostatuslovid: let wostatuslovid):
            return .requestParameters(parameters: ["input":["wostatuslovid":wostatuslovid]], encoding: JSONEncoding.default)
        case .GetWorkOrderDetails(woid: let woid ):
            return .requestParameters(parameters: ["input":["woid":woid]], encoding: JSONEncoding.default)
        case .GetAllBrand():
            return .requestParameters(parameters: ["input":[]], encoding: JSONEncoding.default)
        case .GetModules(brandcd: let brandcd):
            return .requestParameters(parameters: ["input":["brandcd":brandcd]], encoding: JSONEncoding.default)
        case .GetByBrandandModule(brand: let brand, module: let module):
            return .requestParameters(parameters: ["input":["brand":brand,"module":module]], encoding: JSONEncoding.default)
        case .GetBySerialNumber(serialNumber: let serialNumber):
            return .requestParameters(parameters: ["input":["serialNumber":serialNumber]], encoding: JSONEncoding.default)
        case .AddEquipment(dic: let dic):
            print(["input":dic])
            return .requestParameters(parameters: ["input":dic], encoding: JSONEncoding.default)
        case .GetGroup():
            return .requestParameters(parameters: ["input":[]], encoding: JSONEncoding.default)
        case .GetFaults():
            return .requestParameters(parameters:  ["input":[]], encoding: JSONEncoding.default)
        case .GetActionFaults(faultcode: let faultcode):
            return .requestParameters(parameters: ["input":["faultcode" : faultcode]], encoding: JSONEncoding.default)
        case .addUsedItems(let data):
            return .requestParameters(parameters: ["input":data], encoding: JSONEncoding.default)
        case .getStatus(type: let type):
            return .requestParameters(parameters: ["input":["type":type]], encoding: JSONEncoding.default)
        case .UpdateWorkOrderDetails(let data):
            return .requestParameters(parameters: ["input":data], encoding: JSONEncoding.default)


        case .GetItemDescription(query:let query):
            return .requestParameters(parameters: ["input":["itemCode" : query]], encoding: JSONEncoding.default)


        case .SearchBrandByName(query:  let query) :
            return .requestParameters(parameters: ["input":["brandname" : query]], encoding: JSONEncoding.default)
        case .SearchModuleByName(query: let query) :
            return .requestParameters(parameters: ["input":["modulename" : query]], encoding: JSONEncoding.default)

        case .SearchBySerialNumber(query: let query) :
            return .requestParameters(parameters: ["input":["serialnumber" : query]], encoding: JSONEncoding.default)

        case .getContractUnits(contractcd: let contractcd ):
            return .requestParameters(parameters: ["input":["contractcd" : contractcd]], encoding: JSONEncoding.default)
        case .getContractDevices(contractcd: let contractcd , itm_cd: let itm_cd):
            return .requestParameters(parameters: ["input":["contractcd" : contractcd , "itm_cd":itm_cd]], encoding: JSONEncoding.default)
        }
    }

    public var headers: [String : String]? {

        return nil
    }

    
    public var baseURL: URL{
//        return URL(string: "http://api257766-001-site1.ctempurl.com/api/MMSAPI/")!

//        return URL(string: "http://37.34.230.240:81/CoolexAPIv2/api/MMSAPI")!
         return URL(string: "http://62.215.104.108/MMSCoolexAPI/api/MMSAPI/")!
//         return URL(string: "http://amanahadmin-001-site1.itempurl.com/api//MMSAPI/")!

    }
    
    public var path: String{
        
        switch self {
        case .Login(email: _, password: _):
            return "login"


        case .GetWorkOrderDetails(woid: _):
            return "GetWorkOrderDetails"

        case .GetModules(brandcd: _):
            return "GetModules"


        case .AddEquipment(dic:_):
            return "AddEquipment"
        case .GetGroup():
            return "GetGroups"
        case .GetFaults():
            return "GetFaults"
        case .GetActionFaults(faultcode: _):
            return "GetActionFaults"
        case .addUsedItems(_):
            return "AddUsedItems"

        case .UpdateWorkOrderDetails (_):
            return "UpdateWorkOrderDetails"

            // new API
        case .GetByBrandandModule(brand: _, module: _):
            return "GetByBrandandModule"
            
        case .GetBySerialNumber(serialNumber:_):
            return "GetBySerialNumber"
        case .GetAllBrand():
            return "GetBrands"
        case .GetWorkOrderStatusAction(wostatuslovid: _):
            return "GetWorkOrderStatusAction"
        case .getStatus(type: _):
            return "GetLOVByType"
        case .GetWorkOrdersByGroup(groupcd: _):
            return "GetWorkOrdersByGroup"
        case .GetClosedWorkOrdersByGroup(groupcd: _):
            return "GetClosedWorkOrdersByGroup"
        case .GetClosedWorkOrdersbySearch(_):
            return "GetClosedWorkOrdersbySearch"
        case .GetItemDescription(_):
            return "GetItemDescription";


        case .SearchBrandByName(query:  let query) :
            return "SearchBrandByName"
        case .SearchModuleByName(query: let query) :
            return "SearchModuleByName"

        case .SearchBySerialNumber(query: let query) :
            return "SearchBySerialNumber"

        case .getContractUnits(contractcd: _):
            return "GetContractUnits"
        case .getContractDevices(contractcd: _, itm_cd: _):
             return "GetContractDevices"
        }
    }
    
//    public var parameters: [String : Any]?{
//
//        switch self {
//        case .Login(email: let email, password: let pas):
//            return ["input": ["username":email,"password":pas]]
//        case .GetWorkOrdersByGroup(groupcd:let groupcd):
//            return ["input":["groupcd":groupcd]]
//        case .GetClosedWorkOrdersByGroup(groupcd:let groupcd):
//            return ["input":["groupcd":groupcd]]
//        case .GetClosedWorkOrdersbySearch(groupcd:let groupcd ,wono:let  wono,complno:let  complno,custcd:let  custcd) :
//            return ["input":["groupcd":groupcd , "wono":wono , "complno" : complno , "custcd" : custcd]]
//        case .GetWorkOrderStatusAction(wostatuslovid: let wostatuslovid):
//            return ["input":["wostatuslovid":wostatuslovid]]
//        case .GetWorkOrderDetails(woid: let woid ):
//             return ["input":["woid":woid]]
//        case .GetAllBrand():
//            return ["input":[]]
//        case .GetModules(brandcd: let brandcd):
//            return ["input":["brandcd":brandcd]]
//        case .GetByBrandandModule(brand: let brand, module: let module):
//            return ["input":["brand":brand,"module":module]]
//        case .GetBySerialNumber(serialNumber: let serialNumber):
//            return ["input":["serialNumber":serialNumber]]
//        case .AddEquipment(dic: let dic):
//            print(["input":dic])
//            return ["input":dic]
//        case .GetGroup():
//             return ["input":[]]
//        case .GetFaults():
//            return ["input":[]]
//        case .GetActionFaults(faultcode: let faultcode):
//            return ["input":["faultcode" : faultcode]]
//        case .addUsedItems(let data):
//           return ["input":data]
//        case .getStatus(type: let type):
//             return ["input":["type":type]]
//        case .UpdateWorkOrderDetails(let data):
//            return ["input":data]
//
//
//        case .GetItemDescription(query:let query):
//            return ["input":["itemCode" : query]]
//        }
//
//    }
//
//    public var parameterEncoding: ParameterEncoding{
//
//        return JSONEncoding.default
//    }

    public var method: Moya.Method{
        
        switch self {
            
        default:
            return .post
        }
    }
    

    
    public var sampleData: Data{
        switch self {
        default:
            return "".data(using: String.Encoding.utf8)!
        }
    }
    
    public var validate: Bool {
        switch self {
            
        default:
            return false
        }
    }
    
    
}

//private func JSONResponseDataFormatter(_ data: Data) -> Data {
//    do {
//        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
//        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
//        return prettyData
//    } catch {
//        return data // fallback to original data if it can't be serialized.
//    }
//}

