//
//	RootClass.swift
//
//	Create by Amr Abd El-Aal on 27/5/2017
//	Copyright © 2017. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class Group : NSObject, NSCoding{
    
    var employeename : String!
    var employeeno : String!
    var groupcd : String!
    var groupdesc : String!
    var groupid : String!
    var remarks : String!
    var vandesc : String!
    var vanno : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        employeename = json["employeename"].stringValue
        employeeno = json["employeeno"].stringValue
        groupcd = json["groupcd"].stringValue
        groupdesc = json["groupdesc"].stringValue
        groupid = json["groupid"].stringValue
        remarks = json["remarks"].stringValue
        vandesc = json["vandesc"].stringValue
        vanno = json["vanno"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if employeename != nil{
            dictionary["employeename"] = employeename
        }
        if employeeno != nil{
            dictionary["employeeno"]  = employeeno
        }
        if groupcd != nil{
            dictionary["groupcd"]  = groupcd
        }
        if groupdesc != nil{
            dictionary["groupdesc"]  = groupdesc
        }
        if groupid != nil{
            dictionary["groupid"]  = groupid
        }
        if remarks != nil{
            dictionary["remarks"]  = remarks
        }
        if vandesc != nil{
            dictionary["vandesc"]  = vandesc
        }
        if vanno != nil{
            dictionary["vanno"]  = vanno
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        employeename = aDecoder.decodeObject(forKey: "employeename") as? String
        employeeno = aDecoder.decodeObject(forKey: "employeeno") as? String
        groupcd = aDecoder.decodeObject(forKey: "groupcd") as? String
        groupdesc = aDecoder.decodeObject(forKey: "groupdesc") as? String
        groupid = aDecoder.decodeObject(forKey: "groupid") as? String
        remarks = aDecoder.decodeObject(forKey: "remarks") as? String
        vandesc = aDecoder.decodeObject(forKey: "vandesc") as? String
        vanno = aDecoder.decodeObject(forKey: "vanno") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if employeename != nil{
            aCoder.encode(employeename, forKey: "employeename")
        }
        if employeeno != nil{
            aCoder.encode(employeeno, forKey: "employeeno")
        }
        if groupcd != nil{
            aCoder.encode(groupcd, forKey: "groupcd")
        }
        if groupdesc != nil{
            aCoder.encode(groupdesc, forKey: "groupdesc")
        }
        if groupid != nil{
            aCoder.encode(groupid, forKey: "groupid")
        }
        if remarks != nil{
            aCoder.encode(remarks, forKey: "remarks")
        }
        if vandesc != nil{
            aCoder.encode(vandesc, forKey: "vandesc")
        }
        if vanno != nil{
            aCoder.encode(vanno, forKey: "vanno")
        }
        
    }
    
}
