//
//  HistoryCustomeCell.swift
//  Coolex
//
//  Created by Bassem Abbas on 8/16/17.
//  Copyright © 2017 Coolex Dev. All rights reserved.
//

import Foundation
import UIKit
import FoldingCell

class HistoryCustomeCell: FoldingCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    func setupView(name:String){
        nameLabel.text = name
    }
}
